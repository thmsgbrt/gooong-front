import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleNotch } from '@fortawesome/pro-light-svg-icons';

const Loader = () => {
  return <FontAwesomeIcon icon={faCircleNotch} spin={true} size={'6x'} />;
};

export default Loader;
