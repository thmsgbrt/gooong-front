import React, { Component } from 'react';
import StyledLogoInput from '../modules/styledComponents/StyledForm__LogoInput';
import StyledButton from './styled/StyledButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faFileImage,
  faTimesCircle,
  faArrowAltRight,
} from '@fortawesome/pro-light-svg-icons';

class ImageInput extends Component {
  triggerFileInput = () => {
    this.logoInput.click();
  };

  logoSelectedHandler = files => {
    if (files.length > 0) {
      if (files[0].size > this.props.maxSize) {
        alert('File is too big!');
      } else {
        const logoReader = new FileReader();
        logoReader.readAsDataURL(files[0]);
        logoReader.onload = () => {
          this.props.imageSelectHandler(logoReader.result);
        };
      }
    }
  };

  render() {
    return (
      <StyledLogoInput>
        <label htmlFor={this.props.id}>
          <FontAwesomeIcon icon={faFileImage} /> {this.props.label}
        </label>
        <StyledButton
          type="button"
          onClick={() => this.triggerFileInput()}
          className="primary small"
        >
          Choose {this.props.label}
        </StyledButton>
        <span>
          Accepted formats: png, jpg.
          <br />
          Max size: {this.props.maxSizeLabel}
        </span>
        <input
          ref={input => (this.logoInput = input)}
          style={{ display: 'none' }}
          type="file"
          id={this.props.id}
          name={this.props.id}
          accept="image/png, image/jpeg"
          onChange={event => this.logoSelectedHandler(event.target.files)}
        />
        {this.props.currentImage && (
          <div className="form__uploadLogo">
            <img style={{ maxWidth: '200px' }} src={this.props.currentImage} />
            <StyledButton
              type="button"
              onClick={() => this.props.emptyLogoInputState()}
              className="small"
            >
              <FontAwesomeIcon icon={faTimesCircle} size="2x" />
            </StyledButton>
          </div>
        )}
        {!this.props.currentImage && this.props.imageInDB && (
          <div className="form__currentLogo">
            <label>
              <FontAwesomeIcon icon={faArrowAltRight} />
              Current {this.props.label}
            </label>
            <img style={{ maxWidth: '200px' }} src={this.props.imageInDB} />
          </div>
        )}
      </StyledLogoInput>
    );
  }
}

export default ImageInput;
