import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/pro-solid-svg-icons';
import styled from 'styled-components';

const StyledAvatar = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 200px;
  height: 200px;
  background-color: white;
  border-radius: 5px;
  box-shadow: 0px 5px 25px 0px ${props => props.theme.grey900}40;
  margin-bottom: 30px;
  color: ${props => props.theme.purple__logo};
  img {
    width: 100%;
    max-width: 100%;
    max-height: 100%;
  }
`;

const Avatar = props => {
  console.log('AVATAR', props);
  return (
    <StyledAvatar>
      {props.imageSrc && <img src={props.imageSrc} alt={props.imageAlt} />}
      {!props.imageSrc && <FontAwesomeIcon icon={faUser} size={'5x'} />}
    </StyledAvatar>
  );
};

export default Avatar;
