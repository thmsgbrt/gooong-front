import styled from 'styled-components';

const StyledForm = styled.form`
  /* box-shadow: 0 0 5px 3px rgba(0, 0, 0, 0.05);
  
  border: 5px solid white;
  padding: 20px;
  font-size: 1.5rem;
  line-height: 1.5;
  font-weight: 600; */
  background: white;
  padding: 40px 60px;
  border: 1px solid;
  border-radius: 5px;
  label {
    display: block;
    text-align: left;
    margin-top: 1rem;
    font-weight: 700;
    line-height: 3.3rem;
    .svg-inline--fa {
      margin-right: 0.5rem;
      path {
        fill: ${props => props.theme.grey400};
      }
    }
  }
  textarea {
    vertical-align: middle;
    /* max-width: 100%; */
    resize: none;
    min-height: 300px;
  }
  input,
  textarea,
  select {
    width: 100%;
    padding: 0.9rem;
    font-size: 1rem;
    border-radius: 5px;
    border: 1px solid #a0adf9;
    background-color: #f8f9fe;
    font-weight: 100;
    &:focus {
      outline: 0;
      border-color: ${props => props.theme.red};
    }
  }
  .input_x2 {
    input {
      width: 48%;
      &:first-child {
        margin-right: 4%;
      }
    }
  }
  fieldset {
    border: 0;
    padding: 0;

    /* &[disabled] {
      opacity: 0.5;
    }
    &::before {
      height: 10px;
      content: '';
      display: block;
      background-image: linear-gradient(
        to right,
        #ff3019 0%,
        #e2b04a 50%,
        #ff3019 100%
      );
    }
    &[aria-busy='true']::before {
      background-size: 50% auto;
    } */
  }
  button[type='submit'] {
    margin: 4rem 0 1rem;
  }
  .form__currentLogo {
    margin-left: 50px;
    margin-bottom: 30px;
  }
  .form__submit {
    text-align: center;
    &.form__submit--signin {
      button {
        margin-right: 20px;
      }
      a {
        font-size: 0.8rem;
      }
    }
  }
  .form__uploadLogo {
    margin-left: 50px;

    display: flex;
    justify-content: flex-start;
    align-items: flex-start;
    margin-bottom: 30px;
  }
`;

// StyledForm.displayName = 'Form';

export default StyledForm;
