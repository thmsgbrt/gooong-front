import styled from 'styled-components';

const StyledButton = styled.button`
  outline: none;
  cursor: pointer;
  width: auto;
  /* background: ${props => props.theme.blue__flash}; */
  /* color: white; */
  border: 0;
  font-size: 1rem;
  line-height: 1rem;
  padding: 14px 30px;
  border-radius: 10px;
  font-weight: 700;
  transition: all 400ms;
  box-shadow: 0px 0px 0px 0px ${props => props.theme.grey50};

  &.withoutPadding {
    padding: 0;
  }
  &.small {
    font-size: 0.9rem;
    padding: 9px 25px;
  }
  &:disabled {
    color: ${props => props.theme.grey600}!important;
    background: ${props => props.theme.grey200}!important;
    text-shadow: none!important;
  }
  &.animate {
    margin-top: 2px;
    &:hover {
      margin-top: 0px;
      margin-bottom: 2px;
    }
  }
  &.border {
    &:hover {
      border-color: ${props => props.theme.blue__flash};
    }
    border: 1px solid ${props => props.theme.blue__light};
  }
  &.action {
    &.animate {
      &:hover {
        box-shadow: 0px 5px 10px 1px ${props => props.theme.pink100};
      }
    }
    background-color: ${props => props.theme.pink400}
    text-shadow: 0px 0px 10px ${props => props.theme.pink700};
    color: rgba(255, 255, 255);
    box-shadow: 0px 0px 15px ${props => props.theme.grey100};
  }
  &.primary {
    &.animate {
      &:hover {
        box-shadow: 0px 5px 10px 1px ${props => props.theme.blue200};
      }
    }
    text-shadow: 0px 0px 10px ${props => props.theme.blue800};
    background-color: ${props => props.theme.blue500}
    color: rgba(255, 255, 255);
    box-shadow: 0px 0px 15px ${props => props.theme.grey100};
  }
`;
export default StyledButton;
