import React from 'react';
import { Link } from '../routes';
import styled from 'styled-components';

const StyledCta = styled.a`
  display: inline-block;
  cursor: pointer;
  padding: 14px 30px;
  border-radius: 5px;
  font-weight: 700;
  transition: all 400ms;
  margin-bottom: 0;
  font-weight: 700;
  box-shadow: 0px 0px 0px 0px ${props => props.theme.grey50};
  &.withoutPadding {
    padding: 0;
  }
  &.small {
    padding: 9px 25px;
  }
  &.big {
    padding: 25px 80px;
  }
  &.animate {
    margin-top: 2px;
    &:hover {
      margin-top: 0px;
      margin-bottom: 2px;
    }
  }
  &.border {
    &:hover {
      border-color: ${props => props.theme.blue400};
      text-decoration: none;
    }
    border: 1px solid ${props => props.theme.blue__light};
  }
  &.action {
    &.animate {
      &:hover {
        box-shadow: 0px 5px 10px 1px ${props => props.theme.grey400};
      }
    }
    &:hover {
        text-decoration: none!important;
    }
    background-color: ${props => props.theme.pink400};
    /* text-shadow: 0px 0px 10px ${props => props.theme.pink700}; */
    color: rgba(255, 255, 255);
    box-shadow: 0px 0px 15px ${props => props.theme.grey400};
  }
  &.primary {
    &.animate {
      &:hover {
        box-shadow: 0px 5px 10px 1px ${props => props.theme.grey300};
      }
    }
    &:hover {
        text-decoration: none!important;
    }
    text-shadow: 0px 0px 10px ${props => props.theme.blue800};
    background-color: ${props => props.theme.blue400};
    color: rgba(255, 255, 255);
    box-shadow: 0px 0px 15px ${props => props.theme.grey100};
  }
  &.disabled {
    color: ${props => props.theme.grey600};
    background: ${props => props.theme.grey200};
    text-shadow: none;
    pointer-events: none;
  }
`;

const Cta = ({ link, children, variant, fontSize }) => {
  return (
    <Link route={link}>
      <StyledCta style={{ fontSize }} className={variant}>
        {children}
      </StyledCta>
    </Link>
  );
};

export default Cta;
