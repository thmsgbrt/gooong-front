import React, { Component } from 'react';
import { Mutation, ApolloConsumer } from 'react-apollo';

import checkValidity from '../shared/lib/checkValidity';
import updateObject from '../shared/lib/updateObject';

import Input from './Input';
import ErrorMessage from './ErrorMessage';
import ImageInput from './ImageInput';
import StyledForm from './styled/StyledForm';
import StyledButton from './styled/StyledButton';

class Form extends Component {
  state = {
    controls: this.props.controls,
    isFormValid: false,
  };

  componentWillReceiveProps() {
    console.log('bl');
    this.formValidationChecker();
  }

  // componentDidUpdate() {
  //   this.formValidationChecker();
  // }

  inputChangedHandler = (event, controlName) => {
    // Case : confirm password Input
    let confirmSibling = undefined;
    if (this.state.controls[controlName].confirmSiblingControlName) {
      confirmSibling = this.state.controls[
        this.state.controls[controlName].confirmSiblingControlName
      ].value;
    }
    const updatedControls = updateObject(this.state.controls, {
      [controlName]: updateObject(this.state.controls[controlName], {
        value: event.target.value,
        valid: checkValidity(
          event.target.value,
          this.state.controls[controlName].validation,
          confirmSibling
        ),
        touched: true,
      }),
    });
    this.setState({ controls: updatedControls }, () =>
      this.formValidationChecker()
    );
  };

  formValidationChecker = () => {
    const invalidControls = [];
    for (let key in this.state.controls) {
      const control = this.state.controls[key];
      if (!control.valid) {
        invalidControls.push(control.id);
      }
    }
    this.setState({ isFormValid: invalidControls.length === 0 ? true : false });
  };

  getFormData = () => {
    const formData = {};
    for (let key in this.state.controls) {
      if (this.state.controls[key].hasToBeSent) {
        formData[key] = this.state.controls[key].value;
      }
    }
    return this.props.graphqlVariable
      ? { [this.props.graphqlVariable]: formData }
      : formData;
  };

  submitHandler = async (event, mutation) => {
    event.preventDefault();
    const res = await mutation();
    this.props.successHandler(res);
    // props.submitFunc
  };

  render() {
    const formElementsArray = [];
    for (let key in this.state.controls) {
      formElementsArray.push({
        id: key,
        config: this.state.controls[key],
      });
    }

    let form = formElementsArray.map(formElement => (
      <Input
        key={formElement.id}
        label={formElement.config.label}
        elementType={formElement.config.elementType}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        changed={event => this.inputChangedHandler(event, formElement.id)}
      />
    ));

    return (
      <ApolloConsumer>
        {client => (
          <Mutation
            mutation={this.props.mutation}
            variables={this.getFormData()}
          >
            {(mutation, { data, loading, error }) => (
              <StyledForm onSubmit={e => this.submitHandler(e, mutation)}>
                <ErrorMessage error={error} />
                <fieldset disabled={loading} aria-busy={loading}>
                  {form}
                  <StyledButton
                    className={'action'}
                    type="submit"
                    disabled={!this.state.isFormValid}
                  >
                    SUBMIT
                  </StyledButton>
                </fieldset>
              </StyledForm>
            )}
          </Mutation>
        )}
      </ApolloConsumer>
    );
  }
}

export default Form;
