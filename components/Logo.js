import React from 'react';
import styled from 'styled-components';

const LogoDiv = styled.div`
  img {
    display: block; /* Remove extra space between the image */
    width: 100%;
  }
`;

const Logo = () => (
  <LogoDiv className="logo">
    <img src="/static/logo2.svg" alt="Success Book Logo" />
  </LogoDiv>
);

export default Logo;
