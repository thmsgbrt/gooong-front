import gql from 'graphql-tag';

export const CREATE_STORY_MUTATION = gql`
  mutation CreateStory($createStoryInput: CreateStoryInput) {
    createStory(createStoryInput: $createStoryInput) {
      _id
      title
      description
    }
  }
`;

export const UPDATE_STORY_MUTATION = gql`
  mutation updateStory($updateStoryInput: UpdateStoryInput) {
    updateStory(updateStoryInput: $updateStoryInput) {
      _id
      title
      description
      slug
    }
  }
`;

export const DELETE_STORY_MUTATION = gql`
  mutation DeleteStory($storyid: String!) {
    deleteStory(storyid: $storyid) {
      message
    }
  }
`;

export const SINGLE_STORY_QUERY = gql`
  query($singleStoryInput: SingleStoryInput!) {
    singleStory(singleStoryInput: $singleStoryInput) {
      _id
      title
      description
      successes {
        _id
        success_date
        title
        description
        link_text
        link_url
        success_type
        image
      }
      author {
        username
        first_name
        last_name
        image
        website
        stories {
          _id
          title
          successes {
            _id
          }
        }
      }
    }
  }
`;

export const SINGLE_STORY_BY_ID_QUERY = gql`
  query singleStoryById($storyid: String!) {
    singleStoryById(storyid: $storyid) {
      _id
      title
      description
      slug
      successes {
        _id
        success_date
        title
        description
        link_text
        link_url
        success_type
        image
      }
    }
  }
`;
