import gql from 'graphql-tag';

export const CONTACT_FORM_MUTATION = gql`
  mutation ContactForm($contactFormInput: ContactFormInput!) {
    contactForm(contactFormInput: $contactFormInput) {
      message
    }
  }
`;
