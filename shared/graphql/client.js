import gql from 'graphql-tag';

export const TOGGLE_MOBILE_NAV_MUTATION = gql`
  mutation($displayMobileNav: Boolean) {
    toggleMobileNav(displayMobileNav: $displayMobileNav) @client
  }
`;

export const LOGOUT_MUTATION = gql`
  mutation {
    logout @client
  }
`;

export const DISPLAY_MOBILE_NAV_QUERY = gql`
  query {
    displayMobileNav @client
  }
`;
