import gql from 'graphql-tag';

export const CREATE_SUCCESS_MUTATION = gql`
  mutation CreateSuccess($createSuccessInput: CreateSuccessInput) {
    createSuccess(createSuccessInput: $createSuccessInput) {
      description
      link_text
      link_url
      success_date
    }
  }
`;

export const UPDATE_SUCCESS_MUTATION = gql`
  mutation CreateSuccess($updateSuccessInput: UpdateSuccessInput) {
    updateSuccess(updateSuccessInput: $updateSuccessInput) {
      description
      link_text
      link_url
      success_date
    }
  }
`;

export const DELETE_SUCCESS_MUTATION = gql`
  mutation DeleteSuccess($successid: String!) {
    deleteSuccess(successid: $successid) {
      message
    }
  }
`;
