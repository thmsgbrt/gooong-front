import gql from 'graphql-tag';

export const SIGNUP_MUTATION = gql`
  mutation($userInput: SignupInput!) {
    signup(userInput: $userInput) {
      message
    }
  }
`;

export const UPDATE_USER_MUTATION = gql`
  mutation($updateUserInput: UpdateUserInput!) {
    updateUser(updateUserInput: $updateUserInput) {
      message
    }
  }
`;

export const UPDATE_USER_PASSWORD_MUTATION = gql`
  mutation($updateUserPasswordInput: UpdateUserPasswordInput!) {
    updateUserPassword(updateUserPasswordInput: $updateUserPasswordInput) {
      message
    }
  }
`;

export const LOGIN_MUTATION = gql`
  mutation Login($email: String!, $password: String!) {
    login(loginInput: { email: $email, password: $password }) {
      _id
      jwt
    }
  }
`;

export const FORGOT_PASSWORD_MUTATION = gql`
  mutation ForgotPassword($email: String!) {
    forgotPassword(email: $email) {
      message
    }
  }
`;

export const RESET_PASSWORD_MUTATION = gql`
  mutation ResetPassword($resetPasswordToken: String!, $password: String!) {
    resetPassword(
      resetPasswordToken: $resetPasswordToken
      password: $password
    ) {
      message
    }
  }
`;

export const USER_BY_USERNAME_QUERY = gql`
  query UserByUsername($username: String!) {
    userByUsername(username: $username) {
      username
      image
      cover_image
      stories {
        _id
        title
        description
        slug
        successes {
          _id
        }
      }
      website
      social_facebook
      social_twitter
      social_linkedin
      social_twitch
      social_product_hunt
      social_medium
      social_youtube
      social_instagram
    }
  }
`;

export const CURRENT_USER_QUERY = gql`
  query {
    currentUser {
      _id
      first_name
      last_name
      username
      email
      image
      cover_image
      stories {
        _id
        title
        slug
        successes {
          _id
        }
      }
      website
      social_facebook
      social_twitter
      social_linkedin
      social_twitch
      social_product_hunt
      social_medium
      social_youtube
      social_instagram
    }
  }
`;

export const UPDATE_SOCIAL_MUTATION = gql`
  mutation UpdateSocialMutation(
    $updateUserSocialInput: UpdateUserSocialInput!
  ) {
    updateUserSocial(updateUserSocialInput: $updateUserSocialInput) {
      message
    }
  }
`;

export const VALIDATE_ACCOUNT_MUTATION = gql`
  mutation ValidateAccount($token: String!) {
    validateAccount(token: $token) {
      message
    }
  }
`;
