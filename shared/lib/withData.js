import withApollo from 'next-with-apollo';
import ApolloClient from 'apollo-boost';
import environments from '../env.config';
import initialState from '../store/initialState';
import mutation from '../store/mutation';

/**
 *
 *
 *
 * THIS IS NOT USER ANYMORE, CAN BE DELETED
 * THIS IS NOT USER ANYMORE, CAN BE DELETED
 * THIS IS NOT USER ANYMORE, CAN BE DELETED
 * THIS IS NOT USER ANYMORE, CAN BE DELETED
 *
 *
 */

function createClient({ headers }) {
  // Sometime, headers is undefined, so we first check
  // if (headers) {
  //   headers.authorization = 'ta grand mere';
  //   console.log('cooik', document.cookie);
  //   console.log('putain de header', headers);
  // }
  return new ApolloClient({
    uri: environments.API_URL,
    request: operation => {
      operation.setContext({
        fetchOptions: {
          credentials: 'include',
        },
        headers,
      });
    },
    // Local data (redux apollo)
    clientState: {
      resolvers: {
        Mutation: mutation,
      },
      defaults: initialState,
    },
  });
}

export default withApollo(createClient);
