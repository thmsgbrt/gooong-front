const dateFormater = {
  getMonth_DD_YYYY: date => {
    let d = new Date(date);
    var options = { year: 'numeric', month: 'long', day: 'numeric' };
    return d.toLocaleDateString('en-EN', options);
  },
};

export default dateFormater;
