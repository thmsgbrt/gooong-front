import { ApolloClient, InMemoryCache, ApolloLink } from 'apollo-boost';
import { createHttpLink } from 'apollo-link-http';
import { withClientState } from 'apollo-link-state';
import { setContext } from 'apollo-link-context';
import fetch from 'isomorphic-unfetch';
import initialClientState from '../../store/initialState';
import mutation from '../../store/mutation';
import environments from '../../env.config';

let apolloClient = null;
// Polyfill fetch() on the server (used by apollo-client)
if (!process.browser) {
  global.fetch = fetch;
}

function create(initialState, { getToken }) {
  const authLink = setContext((_, { headers }) => {
    const token = getToken();
    return {
      headers: {
        ...headers,
        authorization: token ? token : '',
      },
    };
  });
  const httpLink = createHttpLink({
    uri: environments.API_URL,
    credentials: 'same-origin',
  });

  const stateLink = withClientState({
    resolvers: {
      Mutation: mutation,
    },
    defaults: initialClientState,
  });

  // Check out https://github.com/zeit/next.js/pull/4611 if you want to use the AWSAppSyncClient
  return new ApolloClient({
    connectToDevTools: process.browser,
    ssrMode: !process.browser, // Disables forceFetch on the server (so queries are only run once)
    link: ApolloLink.from([stateLink, authLink.concat(httpLink)]),
    cache: new InMemoryCache().restore(initialState || {}), //  With this, we don't see the unlogged menu first while page is loading when we are connected
    // cache: new InMemoryCache(),
  });
}

export default function initApollo(initialState, options) {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (!process.browser) {
    return create(initialState, options);
  }

  // Reuse client on the client-side
  if (!apolloClient) {
    apolloClient = create(initialState, options);
  }

  return apolloClient;
}
