const routes = require('next-routes');

// prettier-ignore
module.exports = routes()
  .add('index', '/', 'index')
  .add('login', '/login', 'login')
  .add('signup', '/signup', 'signup')
  .add('account-validation', '/account-validation', 'account-validation')
  .add('forgot-password', '/u/forgot-password', 'forgot-password')
  .add('reset-password', '/u/reset-password', 'reset-password')
  .add('dashboard', '/u/dashboard', 'dashboard')
  .add('dashboard/account','/u/dashboard/account','dashboard')
  .add('dashboard/my-stories', '/u/dashboard/stories', 'dashboard')
  .add('dashboard/create-story', '/u/dashboard/create-story', 'dashboard')
  .add('dashboard/update-story','/u/dashboard/update-story/:storyid','dashboard')
  .add('contact', '/contact', 'contact')
  .add('page-not-found', '/page-not-found', 'page-not-found')
  .add('user', '/:username', 'user')
  .add('story', '/:username/:story_slug', 'story');
