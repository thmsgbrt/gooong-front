import React, { Component } from 'react';
import styled, { ThemeProvider, createGlobalStyle } from 'styled-components';
import { DISPLAY_MOBILE_NAV_QUERY } from '../shared/graphql';
import Router from 'next/router';
import * as gtag from '../shared/lib/gtag';
import { Query } from 'react-apollo';
import Meta from '../components/Meta';
import Header from '../modules/header/containers/Header';
import Footer from '../modules/footer/containers/Footer';

Router.events.on('routeChangeComplete', url => gtag.pageview(url));

const theme = {
  max__width: '90rem',
  blue__dark: '#070c23',
  blue__flash: '#1E28FF',
  blue__light: '#E4F0F6',
  purple__dark: '#230046',
  purple__flash: '#8000ff',
  red__flash: '#FF0060',
  red__light: '#fdb3cf',
  golden500: '#FFA700',
  blue500: '#002953',
  // red: '#FF0000',
  grey500: '#393939',
  grey__dark: '#2f2f2f',
  grey: '#5A5A5A',
  grey__light: '#F2F2F2',
  white__off: '#F7F7F7',
  // bs: '0 12px 24px 0 rgba(0, 0, 0, 0.09)',
  green__logo: '#ACEA00',
  purple__logo: '#2C00D1',
  blue50: '#E3F2FD',
  blue100: '#BBDEFB',
  blue200: '#90CAF9',
  blue300: '#64B5F6',
  blue400: '#42A5F5',
  blue500: '#2196F3',
  blue600: '#1E88E5',
  blue700: '#1976D2',
  blue800: '#1565C0',
  blue900: '#0D47A1',
  deeppurple50: '#EDE7F6',
  deeppurple100: '#D1C4E9',
  deeppurple200: '#B39DDB',
  deeppurple300: '#9575CD',
  deeppurple400: '#7E57C2',
  deeppurple500: '#673AB7',
  deeppurple600: '#5E35B1',
  deeppurple700: '#512DA8',
  deeppurple800: '#4527A0',
  deeppurple900: '#311B92',
  indigo50: '#E8EAF6',
  indigo100: '#C5CAE9',
  indigo200: '#9FA8DA',
  indigo300: '#7986CB',
  indigo400: '#5C6BC0',
  indigo500: '#3F51B5',
  indigo600: '#3949AB',
  indigo700: '#303F9F',
  indigo800: '#283593',
  indigo900: '#1A237E',
  pink50: '#FCE4EC',
  pink100: '#F8BBD0',
  pink200: '#F48FB1',
  pink300: '#F06292',
  pink400: '#EC407A',
  pink500: '#E91E63',
  pink600: '#D81B60',
  pink700: '#C2185B',
  pink800: '#AD1457',
  pink900: '#880E4F',
  grey50: '#FAFAFA',
  grey100: '#F5F5F5',
  grey200: '#EEEEEE',
  grey300: '#E0E0E0',
  grey400: '#BDBDBD',
  grey500: '#9E9E9E',
  grey600: '#757575',
  grey700: '#616161',
  grey800: '#424242',
  grey900: '#212121',
};

const StyledPage = styled.div`
  position: relative;
  left: 0px;
  max-width: 100vw;
  transition: left 200ms ease-in;
  overflow: hidden;
  background: ${props => props.theme.grey50};
  &.mobileNavActive {
    position: relative;
    left: calc(0px - 240px);
    overflow: visible;
  }
`;

const GlobalStyle = createGlobalStyle`
  html {
    box-sizing: border-box;
    font-size: 100%;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
  body {
    padding: 0;
    margin: 0;
    font-size: 1.2rem; // 1rem =16px;
    font-family: 'Comfortaa', sans-serif;
    font-weight: 400;
    color: #20232D;
    /* color: ${theme.grey900}; */
  }
  p {
    text-align: left;
    line-height: 1.5;
  }
  a, a:visited {
    color: inherit;
    text-decoration: none;
  }
  a:hover {
    text-decoration: underline;
  }
  .text-center{
    text-align:center;
  }
  .highlight {
    background-image: linear-gradient(0,#58FF00,#8AFF4D 50%,rgba(53,140,91,0) 0);
    /* background-image: linear-gradient(0,${theme.pink200},${
  theme.pink100
} 50%,rgba(53,140,91,0) 0); */
  }
  h1 {
    font-size: 2.5rem;
  }
  h2 {
    font-size: 2rem;
  }
  h1, h2 {
    font-family: 'Open Sans', sans-serif;
  }
  b {
    font-family: 'Open Sans', sans-serif;
    font-weight: 700;
  }
`;

class Page extends Component {
  render() {
    return (
      <>
        <Query query={DISPLAY_MOBILE_NAV_QUERY}>
          {({ data, error, loading }) => {
            return (
              <ThemeProvider theme={theme}>
                <StyledPage
                  className={[
                    data.displayMobileNav ? 'mobileNavActive' : '',
                  ].join(' ')}
                >
                  <Meta />
                  <Header displayMobileNav={data.displayMobileNav} />
                  {/* <Header /> */}
                  {this.props.children}
                  <Footer />
                </StyledPage>
              </ThemeProvider>
            );
          }}
        </Query>
        <GlobalStyle />
      </>
    );
  }
}

export default Page;
