import React, { Component } from 'react';
import { ApolloConsumer } from 'react-apollo';
import Head from 'next/head';

import CurrentUser from '../../_CurrentUser';
import Dashboard__Nav from '../components/Dashboard__Nav';
import Dashboard__Account from '../components/Dashboard__Account';
import Dashboard__Stories from '../components/Dashboard__Stories';
import Dashboard__Story__Form from '../components/Dashboard__Story__Form';
import StyledDashboard from '../styles/StyledDashboard';

class Dashboard extends Component {
  state = {
    currentPage: '',
  };

  componentWillMount(nextProps) {
    this.setCurrentPage(this.props.router.asPath);
  }

  componentWillReceiveProps(newProps) {
    this.setCurrentPage(newProps.router.asPath);
  }

  setCurrentPage = path => {
    if (/u\/dashboard\/create\-story/.test(path)) {
      this.setState({ currentPage: 'create-story' });
    } else if (/u\/dashboard\/update\-story/.test(path)) {
      this.setState({ currentPage: 'update-story' });
    } else if (/u\/dashboard\/account/.test(path)) {
      this.setState({ currentPage: 'account' });
    } else {
      this.setState({ currentPage: '' });
    }
  };

  redirectUserTo = path => {
    this.props.router.push(path); // * Note: doesn't work, lead to 404
    // Router.pushRoute(path);
  };

  render() {
    console.log('test');
    return (
      <ApolloConsumer>
        {client => (
          <CurrentUser>
            {({ data, error, loading }) => {
              const { currentUser } = data;

              if (loading) {
                return <p>loading</p>;
              }

              // Go to login if not logged
              if (!currentUser) {
                this.redirectUserTo('/login');
                return <h1>You're not logged, redirecting to login page...</h1>;
              } else {
                let board;
                // Display the right board depending of the URL
                if (this.state.currentPage === 'create-story') {
                  board = (
                    <Dashboard__Story__Form
                      redirectUserTo={this.redirectUserTo}
                    />
                  );
                } else if (this.state.currentPage === 'update-story') {
                  board = (
                    <Dashboard__Story__Form
                      redirectUserTo={this.redirectUserTo}
                      storyid={this.props.router.query.storyid}
                    />
                  );
                } else if (this.state.currentPage === 'account') {
                  board = <Dashboard__Account />;
                } else {
                  board = (
                    <Dashboard__Stories
                      username={currentUser.username}
                      stories={currentUser.stories}
                    />
                  );
                }
                return (
                  <>
                    <Head>
                      <title>{currentUser.username} - Dashboard</title>
                    </Head>
                    <main>
                      <StyledDashboard>
                        <Dashboard__Nav currentPage={this.state.currentPage} />
                        {board}
                      </StyledDashboard>
                    </main>
                  </>
                );
              }
            }}
          </CurrentUser>
        )}
      </ApolloConsumer>
    );
  }
}

export default Dashboard;
