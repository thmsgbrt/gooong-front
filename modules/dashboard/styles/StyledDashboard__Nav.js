import styled from 'styled-components';

const StyledDashboard__Nav = styled.nav`
  margin-top: 50px;
  ul {
    padding: 0;
    li {
      list-style: none;
      font-weight: 700;
      padding: 9px 15px;
      border-radius: 5px;
      &.current {
        background-color: ${props => props.theme.green__logo};
        a {
          color: white;
        }
      }
      &.disabled {
        color: ${props => props.theme.grey100};
      }
    }
  }
  .title {
    color: #6b808c;
    font-size: 12px;
    font-weight: 400;
    letter-spacing: 0.04em;
    line-height: 1rem;
    margin-top: 16px;
    text-transform: uppercase;
    margin: 0;
    padding: 8px 0;
  }
`;
export default StyledDashboard__Nav;
