import styled from 'styled-components';

const StyledDashboard__Stories = styled.div`
  @media screen and (min-width: 600px) {
    margin-left: 20px;
    border-left: 1px solid ${props => props.theme.grey100};
  }
  section {
    @media screen and (min-width: 600px) {
      margin-left: 100px;
    }
    .section__title {
      display: flex;
      justify-content: flex-start;
      align-items: center;
      h2 {
        display: inline-block;
      }
      h2 + a {
        margin-left: 20px;
      }
    }
  }
`;
export default StyledDashboard__Stories;
