import styled from 'styled-components';

const StyledDashboard__CreateStory = styled.div`
  margin-left: 20px;
  border-left: 1px solid ${props => props.theme.grey100};
  section {
    margin-left: 100px;
    .story {
      display: inline-block;
      width: 200px;
      height: 100px;
      padding: 15px;
      color: ${props => props.theme.blue__dark};
      background-color: ${props => props.theme.white__off};
      margin: 5px;
    }
  }
`;
export default StyledDashboard__CreateStory;
