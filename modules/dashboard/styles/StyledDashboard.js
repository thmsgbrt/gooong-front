import styled from 'styled-components';

const StyledDashboard = styled.div`
  padding-top: 100px;
  padding-bottom: 200px;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: center;
  min-height: 60vh;
  /* background-color: ${props => props.theme.white__off}; */
  nav {
    display:none;
    width: 200px;
    @media screen and (min-width: 600px) {
      display:block;
    }
  }
  > div {
    width: 800px;
  }
`;
export default StyledDashboard;
