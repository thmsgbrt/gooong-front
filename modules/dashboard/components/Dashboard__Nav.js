import React from 'react';
import { Link } from '../../../routes';
import StyledDashboard__Nav from '../styles/StyledDashboard__Nav';

const Dashboard__Nav = ({ currentPage }) => {
  return (
    <StyledDashboard__Nav>
      <ul>
        <li className={currentPage === '' ? 'current' : ''}>
          <Link route="/u/dashboard">
            <a>Stories</a>
          </Link>
        </li>
        <li className={currentPage === 'account' ? 'current' : ''}>
          <Link route="/u/dashboard/account">
            <a>Account settings</a>
          </Link>
        </li>
      </ul>
      {/* <span className="title">
        Entreprise
        <sup>Soon</sup>
      </span>
      <ul>
        <li className="disabled">My Company</li>
      </ul> */}
    </StyledDashboard__Nav>
  );
};

export default Dashboard__Nav;
