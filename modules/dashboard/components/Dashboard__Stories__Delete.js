import React, { Component } from 'react';
import { Mutation, ApolloConsumer } from 'react-apollo';
import { DELETE_STORY_MUTATION } from '../../../shared/graphql';
import { CURRENT_USER_QUERY } from '../../../shared/graphql';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/pro-light-svg-icons';
import StyledButton from '../../../components/styled/StyledButton';
import Modal from '../../../components/Modal';

class Dashboard__Stories__Delete extends Component {
  state = {
    displayModal: false,
  };

  toggleModal = () => {
    this.setState({ displayModal: !this.state.displayModal });
  };

  render = () => {
    return (
      <ApolloConsumer>
        {client => (
          <Mutation
            mutation={DELETE_STORY_MUTATION}
            variables={{ storyid: this.props.storyid }}
          >
            {(deleteStory, { data, error, loading }) => {
              const deleteStoryHandler = async () => {
                const res = await deleteStory();
                // refetch the story
                const refetchStories = await client.query({
                  query: CURRENT_USER_QUERY,
                  fetchPolicy: 'network-only',
                });
              };

              return (
                <>
                  <Modal
                    show={this.state.displayModal}
                    closeModal={this.toggleModal}
                  >
                    <p>Are you sure you want to delete this story?</p>
                    <StyledButton onClick={() => this.toggleModal()}>
                      Nah!
                    </StyledButton>
                    <StyledButton
                      onClick={() => deleteStoryHandler()}
                      className="primary"
                    >
                      Yes, do it!
                    </StyledButton>
                  </Modal>
                  <StyledButton
                    // onClick={() => deleteStoryHandler()}
                    onClick={() => this.toggleModal()}
                    className="withoutPadding"
                  >
                    <FontAwesomeIcon icon={faTrashAlt} />
                  </StyledButton>
                </>
              );
            }}
          </Mutation>
        )}
      </ApolloConsumer>
    );
  };
}

export default Dashboard__Stories__Delete;
