import React, { Component } from 'react';
import { Mutation, ApolloConsumer } from 'react-apollo';
import { CURRENT_USER_QUERY } from '../../../shared/graphql';
import { UPDATE_USER_MUTATION } from '../../../shared/graphql';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faAt } from '@fortawesome/pro-light-svg-icons';

import StyledForm from '../../../components/styled/StyledForm';
import ErrorMessage from '../../../components/ErrorMessage';
import StyledButton from '../../../components/styled/StyledButton';
import ImageInput from '../../../components/ImageInput';

class Dashboard__Account__Form__Profile extends Component {
  state = {
    isFormValid: false,
    first_name: this.props.currentUser ? this.props.currentUser.first_name : '',
    last_name: this.props.currentUser ? this.props.currentUser.last_name : '',
    email: this.props.currentUser ? this.props.currentUser.email : '',
    username: this.props.currentUser ? this.props.currentUser.username : '',
    image: this.props.currentUser ? this.props.currentUser.image : '',
    cover_image: this.props.currentUser
      ? this.props.currentUser.cover_image
      : '',
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value }, () => this.isFormValid());
  };

  updateImageState = (stateName, image) => {
    this.setState({ [stateName]: image });
  };

  emptyImageInputState = stateName => {
    this.setState({ [stateName]: '' });
  };

  isFormValid = () => {
    const inputsState = { ...this.state };
    delete inputsState.isFormValid;
    delete inputsState.image;
    delete inputsState.cover_image;
    const inputsName = Object.keys(inputsState);
    const areInputsFilled = inputsName.every(input => this.state[input] !== '');
    this.setState({
      isFormValid: areInputsFilled,
    });
  };

  getFormData = () => {
    const {
      first_name,
      last_name,
      email,
      username,
      image,
      cover_image,
    } = this.state;
    return { first_name, last_name, email, username, image, cover_image };
  };

  render() {
    return (
      <ApolloConsumer>
        {client => (
          <Mutation
            mutation={UPDATE_USER_MUTATION}
            variables={{ updateUserInput: this.getFormData() }}
          >
            {(update, { data, loading, error }) => (
              <StyledForm
                onSubmit={async e => {
                  // Stop the form from submitting
                  e.preventDefault();
                  // Call the mutation
                  const res = await update();
                  // Manually refresh the CURRENT_USER_QUERY once we set the cookie
                  // (which is needed for the context)
                  const currentUser = await client.query({
                    query: CURRENT_USER_QUERY,
                    fetchPolicy: 'network-only',
                  });
                  // Redirect them to the dashboard page
                  // Router.push({
                  //   pathname: '/item',
                  //   query: { id: res.data.createItem.id },
                  // });
                }}
              >
                {data && data.updateUser && <p>{data.updateUser.message}</p>}
                <ErrorMessage error={error} />
                <fieldset disabled={loading} aria-busy={loading}>
                  <ImageInput
                    id="image"
                    maxSize="524288" // Roughly 500ko
                    maxSizeLabel="500 ko"
                    label="Avatar"
                    imageSelectHandler={base64img =>
                      this.updateImageState('image', base64img)
                    }
                    currentImage={this.state.image}
                    emptyLogoInputState={() =>
                      this.emptyImageInputState('image')
                    }
                    imageInDB={this.state.image}
                  />
                  <ImageInput
                    id="cover_image"
                    maxSize="1048576" // Roughly 1mo
                    maxSizeLabel="1 mo"
                    label="Cover image"
                    imageSelectHandler={e =>
                      this.updateImageState('cover_image', e)
                    }
                    currentImage={this.state.cover_image}
                    emptyLogoInputState={() =>
                      this.emptyImageInputState('cover_image')
                    }
                    imageInDB={this.state.cover_image}
                  />

                  <label htmlFor="first_name">
                    <FontAwesomeIcon icon={faUser} /> First Name*
                  </label>
                  <div>
                    <input
                      type="text"
                      id="first_name"
                      name="first_name"
                      value={this.state.first_name}
                      required
                      onChange={this.handleChange}
                    />
                  </div>
                  <label htmlFor="last_name">
                    <FontAwesomeIcon icon={faUser} /> Last Name*
                  </label>
                  <div>
                    <input
                      type="text"
                      id="last_name"
                      name="last_name"
                      value={this.state.last_name}
                      required
                      onChange={this.handleChange}
                    />
                  </div>
                  <label htmlFor="username">
                    <FontAwesomeIcon icon={faUser} /> Username*
                  </label>
                  <div>
                    <input
                      type="text"
                      id="username"
                      name="username"
                      value={this.state.username}
                      required
                      onChange={this.handleChange}
                    />
                  </div>
                  <label htmlFor="email">
                    <FontAwesomeIcon icon={faAt} /> E-mail*
                  </label>
                  <div>
                    <input
                      type="text"
                      id="email"
                      name="email"
                      placeholder="me@domain.com"
                      value={this.state.email}
                      required
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="form__submit">
                    <StyledButton
                      className={'action'}
                      disabled={!this.state.isFormValid}
                      type="submit"
                    >
                      Update your account
                    </StyledButton>
                  </div>
                </fieldset>
              </StyledForm>
            )}
          </Mutation>
        )}
      </ApolloConsumer>
    );
  }
}

export default Dashboard__Account__Form__Profile;
