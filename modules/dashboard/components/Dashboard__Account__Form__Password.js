import React, { Component } from 'react';
import { UPDATE_USER_PASSWORD_MUTATION } from '../../../shared/graphql';

import Form from '../../../components/Form';

class Dashboard__Account__Form__Password extends Component {
  formControls = {
    oldpassword: {
      id: 'oldpassword',
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: '',
      },
      value: '',
      label: 'Old Password',
      validation: {
        required: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
    newpassword: {
      id: 'newpassword',
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: '',
      },
      value: '',
      label: 'New Password',
      validation: {
        required: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
    confirmpassword: {
      id: 'confirmpassword',
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: '',
      },
      value: '',
      label: 'Confirm your password',
      validation: {
        required: true,
        confirmSibling: true,
      },
      confirmSiblingControlName: 'newpassword',
      hasToBeSent: false,
      valid: false,
      touched: false,
    },
  };

  render() {
    return (
      <Form
        controls={this.formControls}
        mutation={UPDATE_USER_PASSWORD_MUTATION}
        graphqlVariable={'updateUserPasswordInput'}
      />
    );
  }
}

export default Dashboard__Account__Form__Password;
