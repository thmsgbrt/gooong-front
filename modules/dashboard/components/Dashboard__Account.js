import React, { Component } from 'react';
import { ApolloConsumer } from 'react-apollo';
import Head from 'next/head';

import CurrentUser from '../../_CurrentUser';
import Dashboard__Account__Form__Profile from './Dashboard__Account__Form__Profile';
import Dashboard__Account__Form__Password from './Dashboard__Account__Form__Password';
import Dashboard__Account__Form__Social from './Dashboard__Account__Form__Social';
import StyledFormPage from '../../styledComponents/StyledFormPage';

class Dashboard__Account extends Component {
  redirectUserTo = path => {
    this.props.router.push(path);
    // Router.pushRoute(path);
  };

  render() {
    return (
      <ApolloConsumer>
        {client => (
          <CurrentUser>
            {({ data, error, loading }) => {
              const { currentUser } = data;

              // Go to login if not logged
              if (!currentUser) {
                this.redirectUserTo('/login');
                return <h1>You're not logged, redirecting to login page...</h1>;
              }
              return (
                <>
                  <Head>
                    <title>{currentUser.username} - Account</title>
                  </Head>
                  <StyledFormPage>
                    <div>
                      <h1>Manage account</h1>
                      <Dashboard__Account__Form__Profile
                        currentUser={currentUser}
                      />
                      <h2>Update Password</h2>
                      <Dashboard__Account__Form__Password />
                      <h2>Update social Link</h2>
                      <Dashboard__Account__Form__Social
                        currentUser={currentUser}
                      />
                    </div>
                  </StyledFormPage>
                </>
              );
            }}
          </CurrentUser>
        )}
      </ApolloConsumer>
    );
  }
}

export default Dashboard__Account;
