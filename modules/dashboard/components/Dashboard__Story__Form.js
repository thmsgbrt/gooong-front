import React, { Component } from 'react';
import { Mutation, ApolloConsumer } from 'react-apollo';
import {
  CURRENT_USER_QUERY,
  SINGLE_STORY_BY_ID_QUERY,
} from '../../../shared/graphql';
import {
  CREATE_STORY_MUTATION,
  UPDATE_STORY_MUTATION,
} from '../../../shared/graphql';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLink, faRocket, faBookOpen } from '@fortawesome/pro-light-svg-icons';
import StyledForm from '../../../components/styled/StyledForm';
import ErrorMessage from '../../../components/ErrorMessage';
import Loader from '../../../components/Loader';

import StyledDashboard__CreateStory from '../styles/StyledDashboard__Stories';
import StyledButton from '../../../components/styled/StyledButton';

class Dashboard__Story__Form extends Component {
  state = {
    storyInDb: null,
    formValues: {
      title: '',
      slug: '',
      description: '',
    },
  };

  handleChange = e => {
    const updatedFormValues = { ...this.state.formValues };
    const { name, value } = e.target;
    updatedFormValues[name] = value;
    this.setState({ formValues: updatedFormValues });
  };

  setStoryValuesFromDb = data => {
    this.setState({
      storyInDb: data.singleStoryById,
      formValues: {
        storyid: data.singleStoryById._id,
        title: data.singleStoryById.title,
        description: data.singleStoryById.description,
        slug: data.singleStoryById.slug,
      },
    });
  };

  render() {
    return (
      <ApolloConsumer>
        {client => {
          const isEditStory = this.props.storyid ? true : false;
          let formTitle = <h2>Create a new story</h2>;
          let formSubmit = 'Create Story';
          let mutationMethod = CREATE_STORY_MUTATION;
          let mutationVariables = { createStoryInput: this.state.formValues };
          if (isEditStory) {
            // If we end here, we have a storyid so we are editing a story
            formSubmit = 'Update Story';
            formTitle = <h2>Update story</h2>;
            // Change mutation props if we are editing
            mutationMethod = UPDATE_STORY_MUTATION;
            mutationVariables = { updateStoryInput: this.state.formValues };
          }
          // Get story info from DB and update the state
          if (isEditStory && this.state.storyInDb === null) {
            const getStory = client
              .query({
                query: SINGLE_STORY_BY_ID_QUERY,
                variables: { storyid: this.props.storyid },
              })
              .then(({ data }) => {
                this.setStoryValuesFromDb(data);
              });
          }
          return (
            <StyledDashboard__CreateStory>
              <section>
                {formTitle}
                <Mutation
                  mutation={mutationMethod}
                  variables={mutationVariables}
                >
                  {(storyMutation, { data, loading, error }) => {
                    if (loading) {
                      return <Loader />;
                    }
                    return (
                      <StyledForm
                        onSubmit={async e => {
                          // Stop the form from submitting
                          e.preventDefault();
                          // Call the mutation
                          const res = await storyMutation();
                          // Refresh user info with new story
                          const refetchCurrentUser = await client.query({
                            query: CURRENT_USER_QUERY,
                            fetchPolicy: 'network-only',
                          });
                          // Redirect to dashboard or story
                          if (isEditStory) {
                            this.props.redirectUserTo(
                              `/${
                                refetchCurrentUser.data.currentUser.username
                              }/${res.data.updateStory.slug}`
                            );
                          } else {
                            // this.props.redirectUserTo('/u/dashboard'); // * Note: doesn't work, lead to 404
                            this.props.redirectUserTo('/u/dashboard');
                          }
                        }}
                      >
                        <ErrorMessage error={error} />
                        <fieldset disabled={loading} aria-busy={loading}>
                          <label htmlFor="title">
                            <FontAwesomeIcon icon={faRocket} /> Title*
                          </label>
                          <div>
                            <input
                              type="text"
                              id="title"
                              name="title"
                              // placeholder="My awesome story"
                              value={this.state.formValues.title}
                              required
                              onChange={this.handleChange}
                            />
                          </div>
                          <label htmlFor="slug">
                            <FontAwesomeIcon icon={faLink} /> Slug*
                          </label>
                          <div>
                            <input
                              type="text"
                              id="slug"
                              name="slug"
                              // placeholder="my-awesome-story"
                              value={this.state.formValues.slug}
                              required
                              onChange={this.handleChange}
                            />
                          </div>
                          <label htmlFor="description">
                            <FontAwesomeIcon icon={faBookOpen} /> Description*
                          </label>
                          <div>
                            <textarea
                              id="description"
                              name="description"
                              value={this.state.formValues.description}
                              required
                              onChange={this.handleChange}
                            />
                          </div>
                          <div className="form__submit">
                            <StyledButton className={'action'} type="submit">
                              {formSubmit}
                            </StyledButton>
                          </div>
                        </fieldset>
                      </StyledForm>
                    );
                  }}
                </Mutation>
              </section>
            </StyledDashboard__CreateStory>
          );
        }}
      </ApolloConsumer>
    );
  }
}

export default Dashboard__Story__Form;
