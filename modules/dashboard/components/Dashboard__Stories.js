import React from 'react';
import { Link } from '../../../routes';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faEdit } from '@fortawesome/pro-light-svg-icons';

import Dashboard__Stories__Delete from './Dashboard__Stories__Delete';
import Cta from '../../../components/Cta';

import StyledStory__Thumbnails from '../../styledComponents/StyledStory__Thumbnails';
import StyledDashboard__Stories from '../styles/StyledDashboard__Stories';

const Dashboard__Stories = ({ username, stories }) => {
  const storyThumbnails = stories.map(story => (
    <StyledStory__Thumbnails key={`storythumbnail_${story._id}`}>
      <Link
        route="story"
        params={{ username: username, story_slug: story.slug }}
      >
        <a>{story.title}</a>
      </Link>
      <div className="story__actions">
        <Link route="dashboard/update-story" params={{ storyid: story._id }}>
          <a>
            <FontAwesomeIcon icon={faEdit} />
          </a>
        </Link>
        <Dashboard__Stories__Delete storyid={story._id} />
      </div>
    </StyledStory__Thumbnails>
  ));
  return (
    <StyledDashboard__Stories>
      <section>
        <div className="section__title">
          <h2>My stories</h2>
          <Cta link="/u/dashboard/create-story" variant="action small">
            <FontAwesomeIcon icon={faPlus} /> New story
          </Cta>
        </div>
        <div>{storyThumbnails}</div>
      </section>
    </StyledDashboard__Stories>
  );
};

export default Dashboard__Stories;
