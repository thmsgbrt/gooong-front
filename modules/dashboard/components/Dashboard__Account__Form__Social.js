import React, { Component } from 'react';
import { UPDATE_SOCIAL_MUTATION } from '../../../shared/graphql';

import Form from '../../../components/Form';

class Dashboard__Account__Form__Social extends Component {
  formControls = {
    social_facebook: {
      id: 'social_facebook',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '',
      },
      value: this.props.currentUser
        ? this.props.currentUser.social_facebook
        : '',
      label: 'social_facebook',
      validation: {},
      hasToBeSent: true,
      valid: true,
      touched: false,
    },
    social_twitter: {
      id: 'social_twitter',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '',
      },
      value: this.props.currentUser
        ? this.props.currentUser.social_twitter
        : '',
      label: 'social_twitter',
      validation: {},
      hasToBeSent: true,
      valid: true,
      touched: false,
    },
    social_linkedin: {
      id: 'social_linkedin',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '',
      },
      value: this.props.currentUser
        ? this.props.currentUser.social_linkedin
        : '',
      label: 'social_linkedin',
      validation: {},
      hasToBeSent: true,
      valid: true,
      touched: false,
    },
    social_twitch: {
      id: 'social_twitch',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '',
      },
      value: this.props.currentUser ? this.props.currentUser.social_twitch : '',
      label: 'social_twitch',
      validation: {},
      hasToBeSent: true,
      valid: true,
      touched: false,
    },
    social_product_hunt: {
      id: 'social_product_hunt',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '',
      },
      value: this.props.currentUser
        ? this.props.currentUser.social_product_hunt
        : '',
      label: 'social_product_hunt',
      validation: {},
      hasToBeSent: true,
      valid: true,
      touched: false,
    },
    social_medium: {
      id: 'social_medium',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '',
      },
      value: this.props.currentUser ? this.props.currentUser.social_medium : '',
      label: 'social_medium',
      validation: {},
      hasToBeSent: true,
      valid: true,
      touched: false,
    },
    social_youtube: {
      id: 'social_youtube',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '',
      },
      value: this.props.currentUser
        ? this.props.currentUser.social_youtube
        : '',
      label: 'social_youtube',
      validation: {},
      hasToBeSent: true,
      valid: true,
      touched: false,
    },
    social_instagram: {
      id: 'social_instagram',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '',
      },
      value: this.props.currentUser
        ? this.props.currentUser.social_instagram
        : '',
      label: 'social_instagram',
      validation: {},
      hasToBeSent: true,
      valid: true,
      touched: false,
    },
    website: {
      id: 'website',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '',
      },
      value: this.props.currentUser ? this.props.currentUser.website : '',
      label: 'website',
      validation: {},
      hasToBeSent: true,
      valid: true,
      touched: false,
    },
  };

  render() {
    return (
      <Form
        controls={this.formControls}
        mutation={UPDATE_SOCIAL_MUTATION}
        graphqlVariable={'updateUserSocialInput'}
      />
    );
  }
}

export default Dashboard__Account__Form__Social;
