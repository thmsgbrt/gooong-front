import React from 'react';
import styled from 'styled-components';

import Signup__Form from '../components/Signup__Form';

const StyledMain = styled.main`
  display: flex;
  margin: 5rem;
  justify-content: space-around;
  > div {
    width: 40%;
  }
`;

const Signup = props => {
  return (
    <StyledMain>
      <div>
        <h1>Signup to Success Book</h1>
        <Signup__Form router={props.router} />
      </div>
    </StyledMain>
  );
};

export default Signup;
