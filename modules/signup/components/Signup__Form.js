import React, { Component } from 'react';
import { Mutation, ApolloConsumer, withApollo } from 'react-apollo';
import { SIGNUP_MUTATION } from '../../../shared/graphql';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faKey, faAt } from '@fortawesome/pro-light-svg-icons';

import ErrorMessage from '../../../components/ErrorMessage';
import CurrentUser from '../../_CurrentUser';
import StyledForm from '../../../components/styled/StyledForm';
import StyledButton from '../../../components/styled/StyledButton';
import Form from '../../../components/Form';

class Signup__Form extends Component {
  formControls = {
    first_name: {
      id: 'first_name',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '',
      },
      value: '',
      label: 'First name',
      validation: {
        required: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
    last_name: {
      id: 'last_name',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '',
      },
      value: '',
      label: 'Last name',
      validation: {
        required: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
    email: {
      id: 'email',
      elementType: 'input',
      elementConfig: {
        type: 'email',
        placeholder: '',
      },
      value: '',
      label: 'email',
      validation: {
        required: true,
        isEmail: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
    username: {
      id: 'username',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '',
      },
      value: '',
      label: 'Username',
      validation: {
        required: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
    password: {
      id: 'password',
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: '',
      },
      value: '',
      label: 'Password',
      validation: {
        required: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
    confirmpassword: {
      id: 'confirmpassword',
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: '',
      },
      value: '',
      label: 'Confirm your password',
      validation: {
        required: true,
        confirmSibling: true,
      },
      confirmSiblingControlName: 'password',
      hasToBeSent: false,
      valid: false,
      touched: false,
    },
  };

  redirectUserToDashboard = () => {
    this.props.router.push('/u/dashboard');
  };

  render() {
    return (
      <CurrentUser>
        {({ data, error, loading }) => {
          if (data && data.currentUser) {
            this.redirectUserToDashboard();
          }
          return (
            <Form
              controls={this.formControls}
              mutation={SIGNUP_MUTATION}
              graphqlVariable={'userInput'}
            />
          );
        }}
      </CurrentUser>
    );
  }
}

export default withApollo(Signup__Form);
