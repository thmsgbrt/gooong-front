import React from 'react';
import Contact__Form from '../components/Contact__Form';

import StyledFormPage from '../../styledComponents/StyledFormPage';

const Contact = props => (
  <StyledFormPage>
    <div>
      <h1>Contact us 👋 </h1>
      <Contact__Form />
    </div>
  </StyledFormPage>
);

export default Contact;
