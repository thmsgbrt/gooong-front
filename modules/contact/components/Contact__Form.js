import React, { Component } from 'react';
import { CONTACT_FORM_MUTATION } from '../../../shared/graphql';

import Form from '../../../components/Form';

class Contact__Form extends Component {
  formControls = {
    name: {
      id: 'name',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '',
      },
      value: '',
      label: 'Name',
      validation: {
        required: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
    email: {
      id: 'email',
      elementType: 'input',
      elementConfig: {
        type: 'email',
        placeholder: '',
      },
      value: '',
      label: 'email',
      validation: {
        required: true,
        isEmail: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
    message: {
      id: 'message',
      elementType: 'textarea',
      elementConfig: {
        type: '',
        placeholder: '',
      },
      value: '',
      label: 'Your message',
      validation: {
        required: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
  };

  successHandler = () => {
    this.setState({
      name: '',
      email: '',
      message: '',
    });
  };

  render() {
    return (
      <Form
        controls={this.formControls}
        mutation={CONTACT_FORM_MUTATION}
        successHandler={this.successHandler}
        graphqlVariable={'contactFormInput'}
      />
    );
  }
}

export default Contact__Form;
