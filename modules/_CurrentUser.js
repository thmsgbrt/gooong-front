import { Query } from 'react-apollo';
import PropTypes from 'prop-types';
import { CURRENT_USER_QUERY } from '../shared/graphql';

const CurrentUser = props => (
  <Query {...props} query={CURRENT_USER_QUERY} fetchPolicy={'network-only'}>
    {payload => props.children(payload)}
  </Query>
);

CurrentUser.propTypes = {
  children: PropTypes.func.isRequired,
};

export default CurrentUser;
