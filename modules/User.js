import React, { Component } from 'react';
import { Link } from '../routes';
import { Query } from 'react-apollo';
import { USER_BY_USERNAME_QUERY } from '../shared/graphql';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRocket, faBooks } from '@fortawesome/pro-light-svg-icons';
import {
  faFacebook,
  faLinkedin,
  faTwitch,
  faProductHunt,
  faMedium,
  faYoutubeSquare,
  faInstagram,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons';
import StyledUser from './styledComponents/StyledUser';
import { faGlasses } from '@fortawesome/pro-solid-svg-icons';
import Avatar from '../components/Avatar';

class User extends Component {
  state = {
    // storyLink: `https://successbook.app/${this.props.router.query.username}/${
    //   this.props.router.query.story_slug
    // }`,
  };

  render() {
    const { username } = this.props.router.query;
    return (
      <main>
        <Query query={USER_BY_USERNAME_QUERY} variables={{ username }}>
          {({ data, error, loading }) => {
            if (loading) {
              return <h1>Loading...</h1>;
            }

            if (error) {
              this.props.router.push('/page-not-found');
              // Router.pushRoute('/page-not-found');
              return <h1>User not found</h1>;
            }
            const {
              username,
              stories,
              website,
              image,
              cover_image,
              social_facebook,
              social_twitter,
              social_linkedin,
              social_twitch,
              social_product_hunt,
              social_medium,
              social_youtube,
              social_instagram,
            } = data.userByUsername;
            let storyThumbnails;
            if (stories.length > 0) {
              storyThumbnails = stories.map(story => (
                <div
                  className="User__Stories__Container__Story"
                  key={`storythumbnail_${story._id}`}
                >
                  <div>
                    <h3>
                      <Link
                        route="story"
                        params={{ username: username, story_slug: story.slug }}
                      >
                        <a>{story.title}</a>
                      </Link>
                    </h3>
                    <Link
                      route="story"
                      params={{ username: username, story_slug: story.slug }}
                    >
                      <a>
                        <p>{story.description}</p>
                      </a>
                    </Link>
                    <Link
                      route="story"
                      params={{ username: username, story_slug: story.slug }}
                    >
                      <a>
                        <p>
                          <FontAwesomeIcon icon={faGlasses} />
                          {'  '}
                          Read story
                        </p>
                      </a>
                    </Link>
                  </div>
                  <div className="Story__Successes">
                    <span>{story.successes.length}</span>
                    <span>
                      <FontAwesomeIcon icon={faRocket} />
                    </span>
                  </div>
                </div>
              ));
            } else {
              storyThumbnails = (
                <p>This user did not create any story yet 👎</p>
              );
            }

            return (
              <StyledUser>
                <div
                  className="User__Background"
                  style={{
                    backgroundImage: cover_image
                      ? `url(${cover_image})`
                      : `url(/static/account__header__default.jpg)`,
                  }}
                />
                <div className="User__Header">
                  <div className="User__Header__Container">
                    <div className="User__Header__MainDetails">
                      <div className="User__Header__Username">
                        <h1>{data.userByUsername.username}</h1>
                      </div>
                      <div className="User__Header__Social">
                        <p>
                          {website && (
                            <>
                              <a href={website} target="_blank">
                                {website}
                              </a>
                              {' - '}
                            </>
                          )}
                          {social_twitter && (
                            <a href={social_twitter} target="_blank">
                              <FontAwesomeIcon icon={faTwitter} />
                            </a>
                          )}
                          {social_facebook && (
                            <a href={website} target="_blank">
                              <FontAwesomeIcon icon={faFacebook} />
                            </a>
                          )}
                          {social_linkedin && (
                            <a href={website} target="_blank">
                              <FontAwesomeIcon icon={faLinkedin} />
                            </a>
                          )}
                          {social_twitch && (
                            <a href={website} target="_blank">
                              <FontAwesomeIcon icon={faTwitch} />
                            </a>
                          )}
                          {social_product_hunt && (
                            <a href={website} target="_blank">
                              <FontAwesomeIcon icon={faProductHunt} />
                            </a>
                          )}
                          {social_medium && (
                            <a href={website} target="_blank">
                              <FontAwesomeIcon icon={faMedium} />
                            </a>
                          )}
                          {social_youtube && (
                            <a href={website} target="_blank">
                              <FontAwesomeIcon icon={faYoutubeSquare} />
                            </a>
                          )}
                          {social_instagram && (
                            <a href={website} target="_blank">
                              <FontAwesomeIcon icon={faInstagram} />
                            </a>
                          )}
                        </p>
                      </div>
                    </div>
                    <div className="User__Header__Logo">
                      <Avatar imageSrc={image} imageAlt="" />
                    </div>
                  </div>
                </div>
                <div className="User__Stories__Container">
                  <h2>
                    <FontAwesomeIcon icon={faBooks} />
                    {'  '}
                    Stories from {username}.
                  </h2>
                  {storyThumbnails}
                </div>
              </StyledUser>
            );
          }}
        </Query>
      </main>
    );
  }
}

export default User;
