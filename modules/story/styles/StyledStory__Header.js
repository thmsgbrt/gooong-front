import styled from 'styled-components';

const StoryHeader = styled.div`
display: flex;
justify-content: space-between;
max-width: 60rem;
margin: 100px auto;
border-bottom: 1px solid ${props => props.theme.grey200};
.StyledStory__Header__Author {
  display:flex;
  align-items: flex-end;
  flex-direction: column;
  border-right: 1px solid ${props => props.theme.grey200};
  padding: 30px 40px 30px 0;
  .StyledStory__Header__Author__Name {
    .author__username, .author__website {
      display: block;
      text-align: right;
    }
    .author__username {
      font-weight: 700;
      font-size: 1.2rem;
      margin-bottom: 10px;
    }
  }
}
.StyledStory__Header__Title {
  padding-left: 40px;
  h1 {}
}
  /* text-align: center;
  border-top: 1px solid ${props => props.theme.grey100};
  background: radial-gradient(
    at top,
    white,
    white,
    ${props => props.theme.grey100}
  );
  padding: 40px 50px 0;
  @media (min-width: 1000px) {
    padding: 60px 200px 0;
  }
  .story__header__logo {
    max-width: 200px;
  }
  h1 {
    font-family: 'Open Sans', serif;
    font-weight: 400;
    font-size: 40px;
    color: ${props => props.theme.grey__black};
    strong {
      font-weight: 700;
    }
  } */
`;

export default StoryHeader;
