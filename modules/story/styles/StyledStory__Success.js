import styled from 'styled-components';

const StyledStory__Success = styled.section`
  display: grid;
  grid-template-columns: 1px 1fr 1fr;
  grid-template-areas: 'Success__Timeline Success__Container .';
  width: 60%;
  margin: 0 auto;
  .Success__Timeline {
    grid-area: Success__Timeline;
    width: 1px;
    background-color: ${props => props.theme.grey200};
    .Success__Timeline__MainTypeCircle {
      display: none;
    }
  }
  .Success__Container {
    grid-area: Success__Container;
    margin: 0 20px;
    padding: 100px 0;
    box-sizing: border-box;
    .Event__article {
      display: flex;
      flex-direction: column;
      > * {
        margin-left: 25px;
      }
      .Event__article__image {
        display: inline-block;
        max-width: 300px;
        max-height: 200px;
        border-radius: 10px;
        box-shadow: 0px 3px 10px 0px ${props => props.theme.grey200};
        overflow: hidden;
        margin-bottom: 20px;
        img {
          width: 100%;
        }
      }
      .Event__article__date {
        font-size: 0.8rem;
      }
      .Event__edit {
        margin-bottom: 30px;
        button {
          border: none;
          cursor: pointer;
          outline: none;
          background-color: ${props => props.theme.grey50};
          svg {
            height: 1.3rem;
            width: 1.3rem;
            path {
              fill: ${props => props.theme.grey};
            }
          }
        }
      }
      h2 {
        position: relative;
        margin-top: 5px;
        text-transform: uppercase;
        &:before {
          position: absolute;
          content: '';
          display: inline-block;
          width: 10px;
          height: 10px;
          background-color: ${props => props.theme.purple__logo};
          border-radius: 20px;
          left: -30px;
          top: 11px;
        }
      }
    }
  }
  @media (min-width: 1000px) {
    .Success__Container {
      margin: 0;
    }
    &.displayMain {
      grid-template-columns: 1fr 1px 1fr;
      grid-template-rows: 200px 1fr;
      grid-template-areas: '. Success__Timeline .' 'Success__Container Success__Container Success__Container';
      .Success__Timeline {
        position: relative;
        .Success__Timeline__MainTypeCircle {
          display: block;
          width: 30px;
          height: 30px;
          background-color: ${props => props.theme.purple__logo};
          border-radius: 30px;
          box-shadow: 0px 3px 10px 0px ${props => props.theme.grey200};
          position: absolute;
          bottom: 0;
          transform: translateX(-50%);
        }
      }
      .Success__Container {
        text-align: center;
        .Event__article {
          .Event__article__image {
            margin: 0 auto 20px;
          }
          h2 {
            &:before {
              display: none;
            }
          }
          p {
            text-align: center;
          }
        }
      }
    }
    &.displayLeft {
      grid-template-columns: 1fr 1px 1fr;
      grid-template-areas: 'Success__Container Success__Timeline .';
      .Success__Container {
        border-left: none;
        text-align: right;
        .Event__article {
          align-items: flex-end;
          > * {
            margin-left: 0;
            margin-right: 25px;
          }
          h2 {
            &:before {
              left: unset;
              right: -30px;
            }
          }
          p {
            text-align: right;
          }
        }
      }
    }
    &.displayRight {
      grid-template-columns: 1fr 1px 1fr;
      grid-template-areas: '. Success__Timeline Success__Container';
    }
  }
`;

export default StyledStory__Success;
