import styled from 'styled-components';

const StyledStory__CrossSell = styled.div`
  max-width: ${props => props.theme.max__width};
  margin: 100px auto 20px;
  .Others__Stories__Container__Story {
    display: inline-flex;
    justify-content: space-between;
    align-items: center;
    width: 31%;
    padding: 30px 40px;
    border-radius: 5px;
    background-color: white;
    border-left: 1px solid ${props => props.theme.green__logo};
    /* border: 1px solid ${props => props.theme.grey}; */
    box-shadow: 3px 5px 20px 1px ${props => props.theme.grey200};
    margin: 30px 0px;
    .Story__Successes {
      display:flex;
      align-items: center;
      justify-content: space-evenly;
      background-color: ${props => props.theme.purple__logo};
      padding: 20px;
      border-radius: 5px;
      color: white;
      font-weight: 700;
      font-size: 2rem;
      text-align: center;
      /* margin: 20px 0; */
      width: 130px;
      height: 130px;
    }
  }
`;

export default StyledStory__CrossSell;
