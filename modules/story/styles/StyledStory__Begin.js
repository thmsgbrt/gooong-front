import styled from 'styled-components';

const StyledStory__Begin = styled.div`
  margin: 0 auto;
  width: 300px;
  /* heighst: 300px; */
  display: flex;
  align-items: center;
  justify-content: space-around;
  div {
    /* margin: 100px; */
    background-color: ${props => props.theme.green__logo};
    width: 40px;
    height: 40px;
    border-radius: 100px;
    animation: beat 2s ease-out infinite;
    box-shadow: 0 0 0 14px ${props => props.theme.grey50},
      0 0 0 15px ${props => props.theme.grey200},
      0 0 0 23px ${props => props.theme.grey50},
      0 0 0 24px ${props => props.theme.grey200};
  }
  @keyframes beat {
    0% {
      box-shadow: 0 0 0 0px ${props => props.theme.grey50},
        0 0 0 0px ${props => props.theme.grey200},
        0 0 0 1px ${props => props.theme.grey50},
        0 0 0 2px ${props => props.theme.grey200};
    }
    33% {
      box-shadow: 0 0 0 0px ${props => props.theme.grey50},
        0 0 0 1px ${props => props.theme.grey200},
        0 0 0 8px ${props => props.theme.grey50},
        0 0 0 9px ${props => props.theme.grey200};
    }
    66% {
      box-shadow: 0 0 0 7px ${props => props.theme.grey50},
        0 0 0 8px ${props => props.theme.grey200},
        0 0 0 16px ${props => props.theme.grey50},
        0 0 0 17px ${props => props.theme.grey200};
    }
    100% {
      box-shadow: 0 0 0 14px ${props => props.theme.grey50},
        0 0 0 15px ${props => props.theme.grey50},
        0 0 0 23px ${props => props.theme.grey50},
        0 0 0 24px ${props => props.theme.grey50};
    }
  }
`;

export default StyledStory__Begin;
