import styled from 'styled-components';

const StyledStory__Success__Add = styled.div`
  width: 800px;
  margin: 50px auto 0;
  text-align: center;
  button.add {
    border: 2px solid ${props => props.theme.grey200};
    padding: 0;
    border-radius: 50px;
    height: 90px;
    width: 90px;
    background-color: ${props => props.theme.grey50};
    svg {
      height: 3rem;
      width: 3rem !important;
      path {
        transition: 300ms ease all;
        fill: ${props => props.theme.grey200};
      }
    }
    :hover {
      border: 2px solid ${props => props.theme.grey300};
      svg {
        path {
          fill: ${props => props.theme.grey};
        }
      }
    }
  }
`;

export default StyledStory__Success__Add;
