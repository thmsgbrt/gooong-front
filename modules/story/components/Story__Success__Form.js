import React, { Component } from 'react';
import { Mutation, ApolloConsumer } from 'react-apollo';
import {
  CREATE_SUCCESS_MUTATION,
  UPDATE_SUCCESS_MUTATION,
} from '../../../shared/graphql';
import { SINGLE_STORY_QUERY } from '../../../shared/graphql';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faLink,
  faRocket,
  faCalendarAlt,
} from '@fortawesome/pro-light-svg-icons';
import StyledForm from '../../../components/styled/StyledForm';
import StyledButton from '../../../components/styled/StyledButton';
import ErrorMessage from '../../../components/ErrorMessage';
import Loader from '../../../components/Loader';
import ImageInput from '../../../components/ImageInput';

class Story__Success__Form extends Component {
  state = {
    success_id: this.props.successData ? this.props.successData._id : undefined,
    story_id: this.props.story_id,
    title: this.props.successData ? this.props.successData.title : '',
    description: this.props.successData
      ? this.props.successData.description
      : '',
    link_url: this.props.successData ? this.props.successData.link_url : '',
    link_text: this.props.successData ? this.props.successData.link_text : '',
    success_date: this.props.successData
      ? this.props.successData.success_date.substring(0, 10)
      : '',
    success_type: this.props.successData
      ? this.props.successData.success_type
      : '',
    image: this.props.successData ? this.props.successData.image : '',
  };

  componentDidMount = () => {
    this.isFormValid();
  };

  updateImageState = image => {
    this.setState({ image });
  };

  emptyImageInputState = () => {
    this.setState({ image: '' });
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value }, () => this.isFormValid());
  };

  isFormValid = () => {
    const { success_date, description, success_type } = this.state;
    this.setState({
      isFormValid:
        success_date !== '' && description !== '' && success_type !== '',
    });
  };

  getFormData = () => {
    const {
      success_id,
      story_id,
      title,
      description,
      link_url,
      link_text,
      success_date,
      success_type,
      image,
    } = this.state;
    return {
      success_id,
      story_id,
      title,
      description,
      link_url,
      link_text,
      success_date,
      success_type,
      image,
    };
  };

  render() {
    const mutationMethod = !this.props.successData
      ? CREATE_SUCCESS_MUTATION
      : UPDATE_SUCCESS_MUTATION;
    const mutationVariables = !this.props.successData
      ? { createSuccessInput: this.getFormData() }
      : { updateSuccessInput: this.getFormData() };
    return (
      <ApolloConsumer>
        {client => (
          <Mutation mutation={mutationMethod} variables={mutationVariables}>
            {(dispatchMutation, { data, loading, error }) => (
              <StyledForm
                onSubmit={async e => {
                  // Stop the form from submitting
                  e.preventDefault();
                  // Call the mutation
                  const res = await dispatchMutation();
                  console.log('🐶PROPS', this.props);
                  const refreshStory = await client.query({
                    query: SINGLE_STORY_QUERY,
                    fetchPolicy: 'network-only',
                    variables: {
                      singleStoryInput: {
                        username: this.props.username,
                        storySlug: this.props.storySlug,
                      },
                    },
                  });

                  // Close form once we get a successful result from DB
                  if (res.data.createSuccess || res.data.updateSuccess) {
                    this.props.toggleDisplayForm();
                  }

                  // Redirect them to the dashboard page
                  // Router.push({
                  //   pathname: '/item',
                  //   query: { id: res.data.createItem.id },
                  // });
                }}
              >
                <ErrorMessage error={error} />
                {loading && <Loader />}
                <fieldset disabled={loading} aria-busy={loading}>
                  <label htmlFor="success_date">
                    <FontAwesomeIcon icon={faCalendarAlt} /> Date
                  </label>
                  <div>
                    <input
                      type="date"
                      id="success_date"
                      name="success_date"
                      value={this.state.success_date}
                      required
                      onChange={this.handleChange}
                    />
                  </div>
                  <label htmlFor="title">
                    <FontAwesomeIcon icon={faRocket} /> Title
                  </label>
                  <div>
                    <input
                      type="text"
                      id="title"
                      name="title"
                      value={this.state.title}
                      required
                      onChange={this.handleChange}
                    />
                  </div>
                  <label htmlFor="description">
                    <FontAwesomeIcon icon={faRocket} /> Description
                  </label>
                  <div>
                    <textarea
                      id="description"
                      name="description"
                      value={this.state.description}
                      required
                      onChange={this.handleChange}
                    />
                  </div>
                  <label htmlFor="link_text">
                    <FontAwesomeIcon icon={faLink} /> Add a link
                  </label>
                  <div className="input_x2">
                    <input
                      type="text"
                      id="link_text"
                      name="link_text"
                      placeholder="Ex: Discover our new website"
                      value={this.state.link_text}
                      onChange={this.handleChange}
                    />
                    <input
                      type="text"
                      id="link_url"
                      name="link_url"
                      placeholder="https://www.mycompany.com"
                      value={this.state.link_url}
                      onChange={this.handleChange}
                    />
                  </div>
                  <label htmlFor="type_normal">
                    <FontAwesomeIcon icon={faLink} /> Type of Success
                  </label>
                  <div className="input_x2">
                    <input
                      type="radio"
                      name="success_type"
                      id="type_normal"
                      checked={this.state.success_type === 'normal'}
                      value="normal"
                      onChange={this.handleChange}
                    />{' '}
                    Normal
                    <input
                      type="radio"
                      name="success_type"
                      id="type_main"
                      value="main"
                      checked={this.state.success_type === 'main'}
                      onChange={this.handleChange}
                    />{' '}
                    Main
                  </div>
                  <ImageInput
                    imageSelectHandler={this.updateImageState}
                    currentImage={this.state.image}
                    emptyLogoInputState={this.emptyImageInputState}
                    imageInDB={this.state.image}
                  />
                  <div className="form__submit">
                    <StyledButton
                      className="action"
                      type="submit"
                      disabled={!this.state.isFormValid}
                    >
                      {this.props.successData ? 'Update ' : 'Create '}
                      success
                    </StyledButton>
                  </div>
                </fieldset>
              </StyledForm>
            )}
          </Mutation>
        )}
      </ApolloConsumer>
    );
  }
}

export default Story__Success__Form;
