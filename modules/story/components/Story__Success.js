import React, { Component } from 'react';
import VisibilitySensor from 'react-visibility-sensor';
import Confetti from 'react-dom-confetti';
import dateFormater from '../../../shared/lib/dateFormater';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTimesCircle } from '@fortawesome/pro-light-svg-icons';
import Story__Success__Form from './Story__Success__Form';
import Story__Success__Delete from './Story__Success__Delete';

import StyledStory__Success from '../styles/StyledStory__Success';

class Story__Success extends Component {
  state = {
    displayForm: false,
    successVisible: false,
  };

  toggleDisplayForm = () => {
    this.setState({ displayForm: !this.state.displayForm });
  };

  onVisibilityChange = isVisible => {
    this.setState({ successVisible: isVisible });
    // console.log('Element is now %s', isVisible ? 'visible' : 'hidden');
  };

  render() {
    const confettiConfig = {
      angle: 90,
      spread: 160,
      startVelocity: 38,
      elementCount: 98,
      decay: 0.9,
    };
    const { success } = this.props;
    const date = dateFormater.getMonth_DD_YYYY(success.success_date);
    let link;
    if (success.link_url) {
      link = (
        <a href={success.link_url} target="_blank">
          <span role="img" aria-label="Link">
            🔗
          </span>
          {success.link_text}
        </a>
      );
    }
    // Checking what type of success is it
    const displayType =
      success.success_type === 'main'
        ? 'displayMain'
        : this.props.displayLeft
        ? 'displayLeft'
        : 'displayRight';
    return (
      <StyledStory__Success className={displayType}>
        <div className="Success__Timeline">
          <div className="Success__Timeline__MainTypeCircle" />
        </div>
        <div className="Success__Container">
          <div className="Event__article">
            {this.props.allowEdit && (
              <div className="Event__edit">
                <button onClick={() => this.toggleDisplayForm()}>
                  {!this.state.displayForm && <FontAwesomeIcon icon={faEdit} />}
                  {this.state.displayForm && (
                    <FontAwesomeIcon icon={faTimesCircle} />
                  )}
                </button>
                <Story__Success__Delete
                  successid={success._id}
                  storyid={this.props.story_id}
                />
              </div>
            )}
            {!this.state.displayForm && (
              <>
                {success.image && (
                  <div className="Event__article__image">
                    <img src={success.image} alt="" />
                  </div>
                )}
                <span className="Event__article__date">{date}</span>
                <h2>{success.title}</h2>
                <p>{success.description}</p>
                {link}
              </>
            )}
            {this.props.lastSuccess && (
              <VisibilitySensor
                onChange={this.onVisibilityChange}
                offset={{ bottom: 150 }}
              >
                <div style={{ width: '1px', height: '1px', margin: '0 auto' }}>
                  <Confetti
                    active={this.state.successVisible}
                    config={confettiConfig}
                  />
                </div>
              </VisibilitySensor>
            )}
            {this.state.displayForm && (
              <Story__Success__Form
                story_id={this.props.story_id}
                storySlug={this.props.storySlug}
                toggleDisplayForm={this.toggleDisplayForm}
                username={this.props.username}
                successData={success}
              />
            )}
          </div>
        </div>
      </StyledStory__Success>
    );
  }
}

export default Story__Success;
