import React, { Component } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/pro-light-svg-icons';
import Story__Success__Form from './Story__Success__Form';

import StyledButton from '../../../components/styled/StyledButton';
import StyledStory__Success__Add from '../styles/StyledStory__Success__Add';

class Story__Success__Add extends Component {
  state = {
    displayForm: false,
  };

  toggleDisplayForm = () => {
    this.setState({ displayForm: !this.state.displayForm });
  };

  render() {
    return (
      <>
        <StyledStory__Success__Add>
          {this.state.displayForm && (
            <>
              <StyledButton onClick={() => this.toggleDisplayForm()}>
                Close
              </StyledButton>
              <Story__Success__Form
                story_id={this.props.story_id}
                username={this.props.username}
                storySlug={this.props.storySlug}
                toggleDisplayForm={this.toggleDisplayForm}
              />
            </>
          )}
          {!this.state.displayForm && (
            <StyledButton
              className={'add'}
              onClick={() => this.toggleDisplayForm()}
            >
              <FontAwesomeIcon icon={faPlus} />
            </StyledButton>
          )}
        </StyledStory__Success__Add>
      </>
    );
  }
}

export default Story__Success__Add;
