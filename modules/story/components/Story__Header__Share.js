import React, { Component } from 'react';
import { TwitterShareButton, TwitterIcon } from 'react-share';
import styled from 'styled-components';

const ShareContainer = styled.div`
  display: inline-flex;
  align-items: center;
  span {
    font-size: 0.6rem;
    text-transform: uppercase;
    margin-right: 10px;
    line-height: 0rem; // Hummmm...
  }
`;

// const IconContainer = styled.img`
//   max-height: 15px;
//   margin: 0 5px;
//   display: block;
// `;

class Story__Header__Share extends Component {
  render() {
    let title;
    if (this.props.isSuccess) {
      title = `🚀 ${this.props.title} on ${
        this.props.username
      }'s story  on 📚 Success Book`;
    } else {
      title = `🚀 Discover ${this.props.title} on 📚 Success Book`;
    }
    return (
      <ShareContainer>
        <span>Share</span>
        <TwitterShareButton
          url={this.props.url}
          title={title}
          // via={'Guibz16'}
          // hashtags={['tamere']}
          style={{ cursor: 'pointer' }}
        >
          <TwitterIcon
            size={35}
            round={true}
            iconBgStyle={{ fill: 'transparent' }}
            logoFillColor={'#20aaf4'}
          />
        </TwitterShareButton>
        {/* <IconContainer src="/static/social-icons/twitter.svg" alt="Twitter" /> */}
        {/* <IconContainer src="/static/social-icons/facebook.svg" alt="Facebook" /> */}
      </ShareContainer>
    );
  }
}

export default Story__Header__Share;
