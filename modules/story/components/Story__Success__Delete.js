import React, { Component } from 'react';
import { Mutation, ApolloConsumer } from 'react-apollo';
import { DELETE_SUCCESS_MUTATION } from '../../../shared/graphql';
import { SINGLE_STORY_BY_ID_QUERY } from '../../../shared/graphql';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/pro-light-svg-icons';
import StyledButton from '../../../components/styled/StyledButton';
import Modal from '../../../components/Modal';

class Story__Success__Delete extends Component {
  state = {
    displayModal: false,
  };

  toggleModal = () => {
    this.setState({ displayModal: !this.state.displayModal });
  };

  render() {
    return (
      <ApolloConsumer>
        {client => (
          <Mutation
            // {...props}
            mutation={DELETE_SUCCESS_MUTATION}
            variables={{ successid: this.props.successid }}
          >
            {(deleteSucess, { data, error, loading }) => {
              const deleteSuccessHandler = async () => {
                const res = await deleteSucess();
                // refetch the story
                const refetchStory = await client.query({
                  query: SINGLE_STORY_BY_ID_QUERY,
                  variables: { storyid: this.props.storyid },
                  fetchPolicy: 'network-only',
                });
              };

              return (
                <>
                  <Modal
                    show={this.state.displayModal}
                    closeModal={this.toggleModal}
                  >
                    <p>Are you sure you want to delete this success?</p>
                    <StyledButton onClick={() => this.toggleModal()}>
                      Nah!
                    </StyledButton>
                    <StyledButton
                      onClick={() => deleteSuccessHandler()}
                      className="primary"
                    >
                      Yes, do it!
                    </StyledButton>
                  </Modal>
                  <button onClick={() => this.toggleModal()}>
                    <FontAwesomeIcon icon={faTrashAlt} />
                  </button>
                </>
              );
            }}
          </Mutation>
        )}
      </ApolloConsumer>
    );
  }
}

export default Story__Success__Delete;
