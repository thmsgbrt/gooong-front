import React, { Component } from 'react';
import { Query } from 'react-apollo';
import { SINGLE_STORY_QUERY } from '../../../shared/graphql';

import CurrentUser from '../../_CurrentUser';
import Story__Success from '../components/Story__Success';
import Story__Success__Add from '../components/Story__Success__Add';
import StyledStory__Header from '../styles/StyledStory__Header';
import StyledStory__Begin from '../styles/StyledStory__Begin';
import StyledStory__CrossSell from '../styles/StyledStory__CrossSell';
import Loader from '../../../components/Loader';
import Avatar from '../../../components/Avatar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBooks, faRocket } from '@fortawesome/pro-light-svg-icons';

class Story extends Component {
  state = {
    storyLink: `https://successbook.app/${this.props.router.query.username}/${
      this.props.router.query.story_slug
    }`,
  };

  render() {
    const { username, story_slug } = this.props.router.query;
    return (
      <main>
        <Query
          query={SINGLE_STORY_QUERY}
          variables={{
            singleStoryInput: {
              username,
              storySlug: story_slug,
            },
          }}
        >
          {({ data, error, loading }) => {
            if (loading) {
              return <Loader />;
            }

            if (error) {
              this.props.router.push('/page-not-found');
              // Router.pushRoute('/page-not-found');
              return <h1>No story found 😢</h1>;
            }

            const story = data.singleStory;
            if (!story) {
              this.props.router.push('/page-not-found');
              // Router.pushRoute('/page-not-found');
              return <h1>No story found 😢</h1>; // Lead visitor to 404
            }
            return (
              <CurrentUser>
                {({ data, error, loading }) => {
                  // prettier-ignore
                  let isUserCreatorOfThisStory = data && data.currentUser && data.currentUser.username === username;
                  let successes;
                  if (story.successes.length > 0) {
                    successes = story.successes.map((success, index) => (
                      <Story__Success
                        id={success._id}
                        key={success._id}
                        success={success}
                        story_id={story._id}
                        username={username}
                        storySlug={story_slug}
                        storyLink={this.state.storyLink}
                        allowEdit={isUserCreatorOfThisStory}
                        displayLeft={index % 2 == 0}
                        lastSuccess={story.successes.length === index + 1}
                      />
                    ));
                  }
                  /**
                   * Check if user has other stories, if so, display crossSell 😎
                   */
                  let otherStoriesFromThisUser;
                  if (story.author.stories.length > 1) {
                    // Because if length === 1 we are already on this story
                    let otherStories = story.author.stories
                      .filter(crossStory => story._id !== crossStory._id)
                      .map((crossStory, index) => (
                        <div
                          className="Others__Stories__Container__Story"
                          key={crossStory._id}
                        >
                          <h4>{crossStory.title}</h4>
                          <div className="Story__Successes">
                            <span>{crossStory.successes.length}</span>
                            <span>
                              <FontAwesomeIcon icon={faRocket} />
                            </span>
                          </div>
                        </div>
                      ));
                    otherStoriesFromThisUser = (
                      <StyledStory__CrossSell>
                        <h3>
                          <FontAwesomeIcon icon={faBooks} />
                          Check other stories of {username}
                        </h3>
                        {otherStories}
                      </StyledStory__CrossSell>
                    );
                  }
                  return (
                    <>
                      <StyledStory__Header>
                        <div className="StyledStory__Header__Author">
                          <Avatar imageSrc={story.author.image} imageAlt="" />
                          <div className="StyledStory__Header__Author__Name">
                            <span className="author__username">
                              {story.author.username}
                            </span>
                            {story.author.website && (
                              <span className="author__website">
                                <a
                                  href={story.author.website}
                                  title={`${story.author.username}\`s website`}
                                >
                                  {story.author.website.replace(
                                    /^https?\:\/\//,
                                    ''
                                  )}
                                </a>
                              </span>
                            )}
                          </div>
                        </div>
                        <div className="StyledStory__Header__Title">
                          <h1>{story.title}</h1>
                          <p>{story.description}</p>
                        </div>
                        {/* <div>
                        </div>
                        <h1>{story.title}</h1>
                        <p>{story.description}</p>
                        <Story__Header__Share
                          title={story.title}
                          url={this.state.storyLink}
                        /> */}
                      </StyledStory__Header>
                      <StyledStory__Begin>
                        <div />
                      </StyledStory__Begin>
                      {successes}
                      {isUserCreatorOfThisStory && (
                        <Story__Success__Add
                          story_id={story._id}
                          username={username}
                          storySlug={story_slug}
                        />
                      )}
                      {otherStoriesFromThisUser}
                    </>
                  );
                }}
              </CurrentUser>
            );
          }}
        </Query>
      </main>
    );
  }
}

export default Story;
