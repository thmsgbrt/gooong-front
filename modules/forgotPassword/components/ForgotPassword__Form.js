import React, { Component } from 'react';
import { FORGOT_PASSWORD_MUTATION } from '../../../shared/graphql';

import Form from '../../../components/Form';

class ForgotPassword__Form extends Component {
  formControls = {
    email: {
      id: 'email',
      elementType: 'input',
      elementConfig: {
        type: 'email',
        placeholder: 'Mail Address',
      },
      value: '',
      label: 'Email',
      validation: {
        required: true,
        isEmail: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
  };

  render() {
    return (
      <Form controls={this.formControls} mutation={FORGOT_PASSWORD_MUTATION} />
    );
  }
}

export default ForgotPassword__Form;
