import React from 'react';
import styled from 'styled-components';

import ForgotPassword__Form from '../components/ForgotPassword__Form';

const StyledMain = styled.main`
  display: flex;
  margin: 5rem;
  justify-content: space-around;
  > div {
    width: 40%;
  }
`;

const ForgotPassword = props => {
  return (
    <StyledMain>
      <div />
      <div>
        <h1>Forgot your password?</h1>
        <ForgotPassword__Form />
      </div>
    </StyledMain>
  );
};

export default ForgotPassword;
