import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/pro-solid-svg-icons';
import Logo from '../../../components/Logo';
import Cta from '../../../components/Cta';

import StyledFooter from '../styles/StyledFooter';

const Footer = () => {
  return (
    <StyledFooter>
      {/* <div className="footer__links">
        <div className="footer__links__column">
          <span>Project</span>
          <ul>
            <li>Product</li>
            <li>Pricing</li>
          </ul>
        </div>
        <div className="footer__links__column">
          <span>Support</span>
          <ul>
            <li>Get help</li>
            <li>Request a feature</li>
            <li>@Successbook on Twitter</li>
          </ul>
        </div>
      </div> */}
      <div className="footer__support">
        <div>
          <h3>Need help?</h3>
          <p>Fill the form and we will get back to you asap!</p>
        </div>
        <div>
          <Cta link="/contact" variant="primary animate">
            Contact
          </Cta>
        </div>
      </div>
      <div className="footer__credit">
        <div className="footer__credit__copyright">
          <span>© 2019</span> <Logo />
        </div>
        <div className="footer__credit__from">
          Made with <FontAwesomeIcon icon={faHeart} /> in{' '}
          <b>
            Paris
            {/* <span role="img" aria-label="France">
              {' '}
              🇫🇷
            </span> */}
          </b>
        </div>
      </div>
    </StyledFooter>
  );
};

export default Footer;
