import styled from 'styled-components';

const StyledFooter = styled.footer`
  margin: 150px 40px 0;
  /* .footer__links {
    display: flex;
    justify-content: left;
    padding-bottom: 100px;
    background-color: ${props => props.theme.grey50};
    .footer__links__column {
      margin-right: 4rem;
      span {
        text-transform: uppercase;
        font-weight: 700;
      }
      ul {
        padding: 0;
        li {
          margin: 3px 0;
          list-style-type: none;
        }
      }
    }
  } */
  .footer__support {
    max-width: 700px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 3rem auto 2rem;
  }
  .footer__credit {
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-direction: column;
    text-align: center;
    color: ${props => props.theme.grey800};
    font-size: 0.9rem;
    max-width: ${props => props.theme.max__width};
    margin: 0 auto;
    /* padding: 40px 40px 80px; */
    border-top: 1px solid ${props => props.theme.grey100};
    padding: 30px 0;
    @media screen and (min-width: 600px) {
      flex-direction: row;
    }
    .footer__credit__from {
      svg {
        animation: pulse 2s ease-out infinite;
        path {
          fill: ${props => props.theme.pink400};
        }
      }
      @media screen and (min-width: 600px) {
        margin-bottom: 0;
      }
    }
    .footer__credit__copyright {
      display: flex;
      align-items: center;
      justify-content: space-between;
      margin-bottom: 30px;
      @media screen and (min-width: 600px) {
        margin-bottom: 0;
      }
      .logo {
        margin-left: 10px;
        margin-bottom: 6px;
        img {
          display: inherit;
        }
        width: 130px;
      }
    }
  }
  /* > div {
    padding: 40px;
  } */
  @keyframes pulse {
    0% {
      transform: scale(1);
    }
    3.3% {
      transform: scale(1.1);
    }
    16.5% {
      transform: scale(1);
    }
    33% {
      transform: scale(1.1);
    }
    100% {
      transform: scale(1);
    }
  }
`;

export default StyledFooter;
