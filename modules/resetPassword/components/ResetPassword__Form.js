import React, { Component } from 'react';
import { RESET_PASSWORD_MUTATION } from '../../../shared/graphql';

import Form from '../../../components/Form';

class ResetPassword__Form extends Component {
  state = {
    token: this.props.token ? this.props.token : 'notoken',
    password: '',
    confirmPassword: '',
    isFormValid: false,
  };

  formControls = {
    token: {
      id: 'token',
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: '',
      },
      value: this.props.token ? this.props.token : 'notoken',
      label: 'Token',
      validation: {
        required: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
    password: {
      id: 'password',
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: '',
      },
      value: '',
      label: 'Password',
      validation: {
        required: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
    confirmpassword: {
      id: 'confirmpassword',
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: '',
      },
      value: '',
      label: 'Confirm your password',
      validation: {
        required: true,
        confirmSibling: true,
      },
      confirmSiblingControlName: 'password',
      hasToBeSent: false,
      valid: false,
      touched: false,
    },
  };

  render() {
    return (
      <Form controls={this.formControls} mutation={RESET_PASSWORD_MUTATION} />
    );
  }
}

export default ResetPassword__Form;
