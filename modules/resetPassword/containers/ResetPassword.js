import React from 'react';
import ResetPassword__Form from '../components/ResetPassword__Form';

const ResetPassword = props => {
  return <ResetPassword__Form token={props.router.query.token} />;
};

export default ResetPassword;
