import React from 'react';
import { Link } from '../../../routes';
import Cta from '../../../components/Cta';
import styled from 'styled-components';
import CurrentUser from '../../_CurrentUser';
import Logout from '../../Logout';

const NavContainer = styled.div`
  &.mobileNavActive {
    right: -200px;
    nav {
      box-shadow: 0px 0px 22px 6px ${props => props.theme.grey100};
    }
    .transparent {
      opacity: 0;
    }
    .rotateTop {
      transform: rotatez(45deg);
      transform-origin: 0%;
      left: 2px;
    }
    .rotateBottom {
      transform: rotatez(-45deg);
      transform-origin: 15%;
    }
  }
  nav {
    position: absolute;
    left: 100%;
    top: 0;
    display: flex;
    flex-direction: column;
    width: 240px;
    /* background-color: white; */
    padding: 60px 20px;
    box-sizing: border-box;
    ul {
      margin: 0;
      li {
        display: inline-block;
        a,
        button {
          /* color: ${props => props.theme.blue__dark}; */
          display: inline-block;
          font-weight: 400;
          /* text-transform: uppercase; */
          font-size: 1.2rem;
        }
        button {
          font-size: 1.2rem;
          line-height: 1.2rem;
        }
        a:hover {
          text-decoration: underline;
        }
      }
    }
    @media (max-width: 799px) {
      height: 100vh;
      overflow: hidden;
      a,
      button,
      li > div {
        margin: 10px 0px;
      }
    }
    @media (min-width: 800px) {
      position: initial;
      display: block;
      width: auto;
      padding: unset;
      a,
      button,
      li > div {
        margin: 0 40px;
      }
    }
  }
`;

const BurgerContainer = styled.div`
  display: block;
  position: relative;
  cursor: pointer;
  height: 27px;
  width: 33px;
  .burger span {
    display: block;
    background: ${props => props.theme.blue__dark};
    height: 4px;
    width: 33px;
    position: absolute;
    transition: all 0.5s ease;
    border-radius: 10px;
  }
  .burger span:nth-child(1) {
    top: 0px;
  }
  .burger span:nth-child(2) {
    top: 10px;
  }
  .burger span:nth-child(3) {
    top: 20px;
  }
  @media (min-width: 800px) {
    display: none;
  }
`;

const UserThumbnail = styled.div`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
  background-color: ${props => props.theme.grey200};
  color: ${props => props.theme.grey};
  border-radius: 40px;
  font-size: 0.9rem;
  line-height: 0.9rem;
  font-weight: 700;
`;

const Nav = props => {
  return (
    <NavContainer
      className={[props.displayMobileNav ? 'mobileNavActive' : ''].join(' ')}
    >
      <BurgerContainer onClick={props.toggleMobileNav}>
        <div className={'burger'}>
          <span className={'rotateTop'} />
          <span className={'transparent'} />
          <span className={'rotateBottom'} />
        </div>
      </BurgerContainer>
      <nav>
        <CurrentUser>
          {({ data, error, loading }) => {
            if (!data || !data.currentUser) {
              return (
                <ul>
                  {/* <li>
                    <Link route="/">
                      <a>Product</a>
                    </Link>
                  </li>
                  <li>
                    <Link route="pricing">
                      <a>Pricing</a>
                    </Link>
                  </li> */}
                  <li>
                    <Cta link="login" variant="border">
                      Sign in
                    </Cta>
                  </li>
                  <li>
                    <Cta link="signup" variant="action">
                      Sign up
                    </Cta>
                  </li>
                </ul>
              );
            } else {
              return (
                <ul>
                  <li>
                    <Link route="dashboard">
                      <a>My stories</a>
                    </Link>
                  </li>
                  {/* <li>
                    <Cta link="sigin" variant="border">
                      Go Pro
                    </Cta>
                  </li> */}
                  <li>
                    <UserThumbnail>
                      <Link
                        route="user"
                        params={{ username: data.currentUser.username }}
                      >
                        <a>
                          <span>TG</span>
                        </a>
                      </Link>
                    </UserThumbnail>
                  </li>
                  <li>
                    <Logout />
                  </li>
                </ul>
              );
            }
          }}
        </CurrentUser>
      </nav>
    </NavContainer>
  );
};

export default Nav;
