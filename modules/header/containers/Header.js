import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { adopt } from 'react-adopt';
import { TOGGLE_MOBILE_NAV_MUTATION } from '../../../shared/graphql';
import Link from 'next/link';
import styled from 'styled-components';
import Router from 'next/router';
import NProgress from 'nprogress';
import Logo from '../../../components/Logo';
import Nav from '../components/Nav';

Router.onRouteChangeStart = () => {
  NProgress.start();
};

Router.onRouteChangeComplete = () => {
  NProgress.done();
};

Router.onRouteChangeError = () => {
  NProgress.done();
};

const StyledHeader = styled.header`
  display: flex;
  justify-content: space-between;
  padding: 25px 25px;
  align-items: center;
  @media (min-width: 1000px) {
    max-width: ${props => props.theme.max__width};
    padding: 30px 0;
    margin: 0 auto;
  }
`;

const LogoContainer = styled.div`
  cursor: pointer;
  width: 260px;
`;

class Header extends Component {
  state = {
    browserResized: false,
    displayMobileNav: false,
  };

  // prettier-ignore
  Composed = adopt({
    toggleMobileNav: ({ render }) => <Mutation mutation={ TOGGLE_MOBILE_NAV_MUTATION } variables={{displayMobileNav: this.state.displayMobileNav}}>{ render }</Mutation>});

  componentDidMount() {
    // Creating events to close nav on resize
    window.addEventListener('resize', () => this.browserResizedHandler());
    window.addEventListener('orientationchange', () =>
      this.browserResizedHandler()
    );
  }

  componentWillUnmount() {
    // Removing events
    window.removeEventListener('resize', () => this.browserResizedHandler());
    window.removeEventListener('orientationchange', () =>
      this.browserResizedHandler(true)
    );
  }

  browserResizedHandler = unmounting => {
    if (!unmounting) {
      this.setState({
        browserResized: true,
        displayMobileNav: false,
      });
    }
  };

  render() {
    return (
      <this.Composed>
        {({ toggleMobileNav }) => {
          if (this.state.browserResized) {
            toggleMobileNav();
            this.setState({
              browserResized: false,
            });
          }
          return (
            <StyledHeader>
              <Link href="/">
                <LogoContainer>
                  <Logo />
                </LogoContainer>
              </Link>
              <Nav
                displayMobileNav={this.props.displayMobileNav}
                toggleMobileNav={() => {
                  this.setState(
                    { displayMobileNav: !this.state.displayMobileNav },
                    () => {
                      toggleMobileNav();
                    }
                  );
                }}
              />
            </StyledHeader>
          );
        }}
      </this.Composed>
    );
  }
}

export default Header;
