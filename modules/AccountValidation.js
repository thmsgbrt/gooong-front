import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { VALIDATE_ACCOUNT_MUTATION } from '../shared/graphql';
import ErrorMessage from './ErrorMessage';
import StyledForm from './styledComponents/Form';
import StyledButton from './styledComponents/StyledButton';

class AccountValidation extends Component {
  state = {
    token: this.props.router.query.resetToken,
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  render() {
    return (
      <Mutation
        mutation={VALIDATE_ACCOUNT_MUTATION}
        variables={{ token: this.state.token }}
      >
        {(validateAccount, { data, loading, error }) => (
          <div>
            <h1>Valid account</h1>
            {data && data.validateAccount && (
              <p>{data.validateAccount.message}</p>
            )}
            {!data && (
              <StyledForm
                onSubmit={async e => {
                  // Stop the form from submitting
                  e.preventDefault();
                  // Call the mutation
                  const res = await validateAccount();
                }}
              >
                <ErrorMessage error={error} />

                <fieldset disabled={loading} aria-busy={loading}>
                  <div>
                    <input
                      type="text"
                      id="token"
                      name="token"
                      placeholder="Please enter a token"
                      value={this.state.token}
                      required
                      onChange={this.handleChange}
                    />
                  </div>
                  <StyledButton className={'action'} type="submit">
                    Valid my account
                  </StyledButton>
                </fieldset>
              </StyledForm>
            )}
          </div>
        )}
      </Mutation>
    );
  }
}

export default AccountValidation;
