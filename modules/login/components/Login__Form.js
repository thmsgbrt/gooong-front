import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import { CURRENT_USER_QUERY } from '../../../shared/graphql';
import { LOGIN_MUTATION } from '../../../shared/graphql';
import { Link } from '../../../routes';

import Form from '../../../components/Form';
import CurrentUser from '../../_CurrentUser';

class Login__Form extends Component {
  formControls = {
    email: {
      id: 'email',
      elementType: 'input',
      elementConfig: {
        type: 'email',
        placeholder: 'Mail Address',
      },
      value: '',
      label: 'Email testacc1@gmail.com',
      validation: {
        required: true,
        isEmail: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
    password: {
      id: 'password',
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: 'Password',
      },
      value: '',
      label: 'password bro!çàJHK!ç78',
      validation: {
        required: true,
      },
      hasToBeSent: true,
      valid: false,
      touched: false,
    },
  };

  redirectUserToDashboard = () => {
    this.props.router.push('/u/dashboard');
  };

  successHandler = async res => {
    document.cookie = `token=Bearer ${res.data.login.jwt};path=/`;
    await this.props.client.query({
      query: CURRENT_USER_QUERY,
      fetchPolicy: 'network-only',
    });
  };

  render() {
    return (
      <CurrentUser>
        {({ data, error, loading }) => {
          if (data && data.currentUser) {
            this.redirectUserToDashboard();
          }
          return (
            <>
              <Form
                controls={this.formControls}
                successHandler={this.successHandler}
                mutation={LOGIN_MUTATION}
              />
              <Link route="forgot-password">
                <a>Forgot password?</a>
              </Link>
            </>
          );
        }}
      </CurrentUser>
    );
  }
}

export default withApollo(Login__Form);
// export default WithApolloClient();
