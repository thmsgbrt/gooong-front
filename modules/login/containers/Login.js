import React from 'react';
import styled from 'styled-components';

import Login__Form from '../components/Login__Form';

const StyledMain = styled.main`
  display: flex;
  margin: 5rem;
  justify-content: space-around;
  > div {
    width: 40%;
  }
`;

const Login = props => {
  return (
    <StyledMain>
      <div>
        <h1>Login</h1>
        <Login__Form router={props.router} />
      </div>
    </StyledMain>
  );
};

export default Login;
