import React, { Component } from 'react';
import StyledHome from '../styles/StyledHome';
import VisibilitySensor from 'react-visibility-sensor';
import Confetti from 'react-dom-confetti';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faRocket,
  faUserChart,
  faUsers,
} from '@fortawesome/pro-light-svg-icons';
import { faDotCircle, faEllipsisH } from '@fortawesome/pro-solid-svg-icons';
// import { faMedal, faTrophyAlt } from '@fortawesome/pro-solid-svg-icons';
import Cta from '../../../components/Cta';

class Home extends Component {
  state = {
    successVisible: false,
  };

  onVisibilityChange = isVisible => {
    this.setState({ successVisible: isVisible });
  };

  render() {
    const confettiConfig = {
      angle: 90,
      spread: 160,
      startVelocity: 38,
      elementCount: 98,
      decay: 0.9,
    };
    return (
      <StyledHome>
        <section className="section--2blocks section__intro">
          <div>
            <div>
              <h1>
                Share your <span className="highlight">growth</span>
                <br />
                with everyone.
              </h1>
              <p>
                Sky rocketting company willing to share your achievements?
                <br />
                Use successbook to talk about the milestones:
              </p>
              <ul>
                <li>
                  <FontAwesomeIcon
                    fixedWidth={true}
                    size="2x"
                    color={'#673AB7'}
                    icon={faRocket}
                  />
                  <b>New features</b> in your product
                </li>
                <li>
                  <FontAwesomeIcon
                    fixedWidth="true"
                    size="2x"
                    color={'#FFA700'}
                    icon={faUserChart}
                  />
                  Increasing <b>MRR</b> (Monthly Recurrent Revenue)
                </li>
                <li>
                  <FontAwesomeIcon
                    fixedWidth="true"
                    size="2x"
                    color={'#ACEA00'}
                    icon={faUsers}
                  />
                  <b>Team</b> growth
                </li>
                <li>
                  {' '}
                  <FontAwesomeIcon
                    fixedWidth="true"
                    size="2x"
                    color={'#E0E0E0'}
                    icon={faEllipsisH}
                  />
                </li>
              </ul>
              {/* <div className="text-center">
                <Cta link="signup" variant="primary">
                  Try Now
                </Cta>
              </div> */}
            </div>
            <div>
              <img src="/static/home/promo-photo.png" alt="Success Book" />
              <VisibilitySensor
                onChange={this.onVisibilityChange}
                offset={{ bottom: 150 }}
              >
                <div
                  style={{
                    width: '1px',
                    height: '1px',
                    margin: '0 auto',
                    position: 'relative',
                    top: '-200px',
                  }}
                >
                  <Confetti
                    active={this.state.successVisible}
                    config={confettiConfig}
                  />
                </div>
              </VisibilitySensor>
            </div>
          </div>
        </section>
        <section className="section__pricing">
          <h2>Plans</h2>
          <div className="section__pricing__plans_container">
            <div className="plan">
              <h3 className="plan__title">Starter</h3>
              {/* <span className="plan__icon">
                <FontAwesomeIcon icon={faMedal} />
              </span> */}
              <ul>
                <li>
                  <FontAwesomeIcon
                    fixedWidth="true"
                    size="xs"
                    icon={faDotCircle}
                  />
                  <b className="highlight">Unlimited</b> stories
                </li>
                <li>
                  <FontAwesomeIcon
                    fixedWidth="true"
                    size="xs"
                    icon={faDotCircle}
                  />
                  <b className="highlight">Unlimited</b> successes
                </li>
              </ul>
              <div className="plan__price">
                <span className="plan__price__amount">Free</span>
              </div>
              <div className="plan__cta">
                <Cta link="signup" variant="primary">
                  Sign up
                </Cta>
              </div>
            </div>
            <div className="plan">
              <h3 className="plan__title">
                Pro <sup>(soon)</sup>
              </h3>
              {/* <span className="plan__icon">
                <FontAwesomeIcon icon={faTrophy} />
              </span> */}
              <ul>
                <li>
                  <FontAwesomeIcon
                    fixedWidth="true"
                    size="xs"
                    icon={faDotCircle}
                  />
                  Successes with <b className="highlight">slideshow</b> or{' '}
                  <b>video</b>
                </li>
                <li>
                  <FontAwesomeIcon
                    fixedWidth="true"
                    size="xs"
                    icon={faDotCircle}
                  />
                  <b className="highlight">Private</b> stories
                </li>
                <li>
                  <FontAwesomeIcon
                    fixedWidth="true"
                    size="xs"
                    icon={faDotCircle}
                  />
                  <b>Automatic</b> sharing on Slack
                </li>
                <li>
                  <FontAwesomeIcon size="xs" icon={faDotCircle} />
                  <b className="highlight">Color personalization</b> of your
                  stories
                </li>
              </ul>
              <div className="plan__price">
                <span className="plan__price__amount">3€</span>
                <span className="plan__price__billed">
                  / month - Billed anually
                </span>
              </div>
              <div className="plan__cta">
                <Cta link="signup" variant="disabled">
                  Sign up
                </Cta>
              </div>
            </div>
          </div>
        </section>
        <section className="section__signup">
          <h2>Try successbook now!</h2>
          <Cta link="signup" variant="primary animate big" fontSize={'1.7rem'}>
            Sign up now!
          </Cta>
        </section>
      </StyledHome>
    );
  }
}

export default Home;
