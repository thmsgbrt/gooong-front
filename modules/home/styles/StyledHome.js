import styled from 'styled-components';

const StyledHome = styled.main`
  max-width: ${props => props.theme.max__width};
  margin: 0 auto;
  section {
    /* padding: 30px; */
    > div {
      display: flex;
      justify-content: space-between;
      flex-direction: column-reverse;
      text-align: center;
      @media screen and (min-width: 900px) {
        text-align: left;
        flex-direction: row;
      }
    }
    &.section--2blocks > div > div {
      &:first-child {
        @media screen and (min-width: 900px) {
          width: 50%;
        }
      }
      &:last-child {
        @media screen and (min-width: 900px) {
          /* width: 50%; */
          img {
            width: 100%;
            max-width: 950px;
          }
        }
      }
    }
    ul {
      padding: 0;
      li {
        list-style-type: none;
        margin: 20px 0;
        .svg-inline--fa {
          /* width: 20px; */
          margin-right: 20px;
          /* path {
            fill: ${props => props.theme.purple__logo};
          } */
        }
      }
    }
  }
  .section__intro {
    margin-top: 90px;
    margin-bottom: 120px;
  }
  .section__pricing {
    h2 {
      text-align: center;
    }
    .section__pricing__plans_container {
      margin: 70px 0;
      display: flex;
      justify-content: space-evenly;
      align-items: flex-start;
      .plan {
        background-color: white;
        box-shadow: 0px 5px 25px 0px ${props => props.theme.grey300}40;
        padding: 15px 70px 50px;
        .plan__title {
          font-size: 2rem;
          font-weight: 700;
          font-family: 'Open Sans', sans-serif;
          text-align: center;
          margin-bottom: 60px;
        }
        .plan__price {
          margin-top: 40px;
          text-align: center;
          span.plan__price__amount {
            font-family: 'Open Sans', sans-serif;
            font-weight: 700;
            font-size: 3rem;
            text-transform: uppercase;
            margin-right: 20px;
          }
          span.plan__price__billed {
            font-size: 0.9rem;
          }
        }
        .plan__cta {
          margin: 40px 0 0 0;
          a {
            display: block;
            text-align: center;
          }
        }
      }
    }
  }
  .section__signup {
    margin: 110px auto;
    padding: 120px 0;
    text-align: center;
    /* background-color: ${props => props.theme.grey500}; */
    background-color: white;
    /* color: white; */
    background-image: url('/static/home/pattern-5.png');
    box-shadow: 0px 5px 25px 0px ${props => props.theme.grey200}40;
    h2 {
      font-size: 2.3rem;
      margin-bottom: 40px;
    }
  }
  @keyframes wizz {
    50% {
      -webkit-transform: translateX(3px) rotate(2deg);
      transform: translateX(3px) rotate(2deg);
    }

    100% {
      -webkit-transform: translateX(-3px) rotate(-2deg);
      transform: translateX(-3px) rotate(-2deg);
    }
  }
`;

export default StyledHome;
