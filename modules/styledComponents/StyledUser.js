import styled from 'styled-components';

const StyledUser = styled.div`
  .User__Background {
    height: 30rem;
    background-size: cover;
    background-position: 50%;
    position: relative;
    color: white;
    > * {
      z-index: 1;
    }
    :after {
      position: absolute;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, 0.3);
      content: ' ';
    }
  }
  .User__Header {
    /* background-color: ${props => props.theme.grey100}; */
    position: relative;
    top: -5rem;
    margin-bottom: 100px;
    .User__Header__Container {
      position: relative;
      display: flex;
      color: white;

      .User__Header__Username {
        text-transform: capitalize;
        h1 {
          font-size: 1.9rem;
          margin:0;
        }
      }
      .User__Header__Social {
        p {
          margin: 0;
          font-size: 1.2rem;
        }
        a {
          .svg-inline--fa {
            margin: 0px 6px;
          }
        }
      }

      .User__Header__MainDetails {
        padding-left: 230px;
        display: flex;
        flex-direction: column;
        /* justify-content: left; */
      }
    }
    .User__Header__Logo {
      transform: translateY(-15%);
      position: absolute;
      display: inline-block;
      display: flex;
      align-items: center;
    }
  }
  .User__Header__Container,
  .User__Stories__Container {
    max-width: ${props => props.theme.max__width};
    margin: 0 auto;
  }
  .User__Stories__Container__Story {
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    padding: 30px 40px;
    border-radius: 5px;
    background-color: white;
    border-left: 1px solid ${props => props.theme.green__logo};
    /* border: 1px solid ${props => props.theme.grey}; */
    box-shadow: 3px 5px 20px 1px ${props => props.theme.grey200};
    margin: 30px 0px;
    .Story__Successes {
      display:flex;
      align-items: center;
      justify-content: space-evenly;
      background-color: ${props => props.theme.purple__logo};
      padding: 20px;
      border-radius: 5px;
      color: white;
      font-weight: 700;
      font-size: 2rem;
      text-align: center;
      /* margin: 20px 0; */
      width: 130px;
      height: 130px;
    }
  }
`;

export default StyledUser;
