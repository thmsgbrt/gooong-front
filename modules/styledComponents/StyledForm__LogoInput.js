import styled from 'styled-components';

const StyledLogoInput = styled.div`
  display: flex;
  justify-content: left;
  align-items: center;
  > label {
    margin-top: 0 !important;
    display: inline-block;
  }
  > button {
    margin-left: 20px;
  }
  span {
    margin-left: 20px;
    font-size: 0.7rem;
  }
  /* display: inline; */
  input[type='file'] {
    display: none;
  }
`;

export default StyledLogoInput;
