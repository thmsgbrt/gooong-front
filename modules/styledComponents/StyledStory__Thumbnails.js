import styled from 'styled-components';

const StyledStory__Thumbnails = styled.div`
  display: inline-flex;
  flex-direction: column;
  justify-content: space-between;
  width: 200px;
  height: 100px;
  padding: 15px;
  border-radius: 5px;
  color: ${props => props.theme.blue__dark};
  border: 1px solid ${props => props.theme.grey};
  /* box-shadow: 2px 2px 5px 0px #d2d2d2; */
  margin: 5px;
  a {
    &:hover {
      text-decoration: underline;
    }
  }
  .story__actions {
    display: none;
    /* cursor: pointer; */
    text-align: center;
  }
  &:hover {
    .story__actions {
      display: flex;
      justify-content: space-evenly;
    }
  }
`;

export default StyledStory__Thumbnails;
