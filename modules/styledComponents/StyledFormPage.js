import styled from 'styled-components';

const StyledFormPage = styled.main`
  display: flex;
  margin: 5rem;
  justify-content: space-around;
  > div {
    width: 40%;
  }
`;

export default StyledFormPage;
