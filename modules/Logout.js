import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { LOGOUT_MUTATION } from '../shared/graphql';
import { CURRENT_USER_QUERY } from '../shared/graphql';
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt } from '@fortawesome/pro-light-svg-icons';

const StyledButton = styled.button`
  padding: 0;
  border: none;
  outline: none;
  font: inherit;
  color: inherit;
  background: none;
  cursor: pointer;
  padding: 5px;
`;

class Logout extends Component {
  resetStore = client => {
    client.resetStore();
  };

  render() {
    return (
      <Mutation
        mutation={LOGOUT_MUTATION}
        refetchQueries={[
          { query: CURRENT_USER_QUERY, fetchPolicy: 'network-only' },
        ]}
      >
        {(logout, res) => {
          return (
            <StyledButton
              onClick={() => {
                logout(), this.resetStore(res.client);
              }}
            >
              <FontAwesomeIcon icon={faSignOutAlt} />
            </StyledButton>
          );
        }}
      </Mutation>
    );
  }
}

export default Logout;
