import gql from 'graphql';
import { DISPLAY_MOBILE_NAV_QUERY } from '../shared/graphql';

const mutation = {
  toggleMobileNav(_, variables, client) {
    const cache = client.cache;
    const { displayMobileNav } = cache.readQuery({
      query: DISPLAY_MOBILE_NAV_QUERY,
    });
    const updatedState = {
      data: {
        displayMobileNav: variables.displayMobileNav,
      },
    };
    cache.writeData(updatedState);
    return updatedState;
  },
  logout(_, variables, client) {
    // Remove cookie
    document.cookie = 'token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
    // Clear store
    // client.resetStore(); // why dont we do it here ? (check logout component)
    return null;
  },
};

export default mutation;
