import Home from '../modules/home/containers/Home';

const HomePage = props => <Home router={props.router} />;

export default HomePage;
