import AccountValidation from '../modules/AccountValidation';

const AccountValidationPage = props => (
  <AccountValidation router={props.router} />
);

export default AccountValidationPage;
