import Signup from '../modules/signup/containers/Signup';

const SignupPage = props => <Signup router={props.router} />;

export default SignupPage;
