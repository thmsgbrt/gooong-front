import User from '../modules/User';

const UserPage = props => <User router={props.router} />;

export default UserPage;
