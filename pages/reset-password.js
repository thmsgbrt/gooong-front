import ResetPassword from '../modules/resetPassword/containers/ResetPassword';

const ResetPasswordPage = props => <ResetPassword router={props.router} />;

export default ResetPasswordPage;
