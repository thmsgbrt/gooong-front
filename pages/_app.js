import App, { Container } from 'next/app';
import Page from '../modules/Page';
import { ApolloProvider } from 'react-apollo';

import withApollo from '../shared/lib/withApollo';

class MyApp extends App {
  render() {
    const { Component, pageProps, apolloClient, router } = this.props;
    return (
      <Container>
        <ApolloProvider client={apolloClient}>
          <Page>
            <Component {...pageProps} router={router} />
          </Page>
        </ApolloProvider>
      </Container>
    );
  }
}

export default withApollo(MyApp);
