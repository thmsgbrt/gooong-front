import Login from '../modules/login/containers/Login';

const LoginPage = props => <Login router={props.router} />;

export default LoginPage;
