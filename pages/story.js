import Story from '../modules/story/containers/Story';

const StoryPage = props => <Story router={props.router} />;

export default StoryPage;
