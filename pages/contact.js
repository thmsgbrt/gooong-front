import Contact from '../modules/contact/containers/Contact';

const ContactPage = props => <Contact router={props.router} />;

export default ContactPage;
