import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';
import { ServerStyleSheet } from 'styled-components';
import { GA_TRACKING_ID } from '../shared/lib/gtag';

export default class MyDocument extends Document {
  static async getInitialProps({ renderPage }) {
    const sheet = new ServerStyleSheet();
    const page = renderPage(App => props =>
      sheet.collectStyles(<App {...props} />)
    );
    const styleTags = sheet.getStyleElement();
    return { ...page, styleTags };
  }

  render() {
    console.log('💩');
    // make the environment available on the client
    const envScript = `window.ENV = '${process.env.WILD_ENV ||
      'development'}';`;
    return (
      <html lang="en">
        <Head>
          {/* <script
            dangerouslySetInnerHTML={{
              __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MHKJ23B');`,
            }}
          /> */}

          <script
            async
            src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`}
          />

          <script
            dangerouslySetInnerHTML={{
              __html: `  window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
            
              gtag('config', '${GA_TRACKING_ID}');`,
            }}
          />

          <link
            href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700|Open+Sans:300,400,500,700"
            rel="stylesheet"
          />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
          <link rel="stylesheet" type="text/css" href="/static/normalize.css" />
          <link rel="stylesheet" type="text/css" href="/static/nprogress.css" />
          {/* <link rel="stylesheet" href="/static/style.css" /> */}
          {/* <link rel="shortcut icon" href="/static/favicon.ico" /> */}
          <script dangerouslySetInnerHTML={{ __html: envScript }} />
          {/* Below line is to make sure CSS loads when we hard reload the page -> https://dev.to/aprietof/nextjs--styled-components-the-really-simple-guide----101c */}
          {this.props.styleTags}
        </Head>
        <body>
          {/* <noscript>
            <iframe
              src="https://www.googletagmanager.com/ns.html?id=GTM-MHKJ23B"
              height="0"
              width="0"
              style={{ display: 'none', visibility: 'hidden' }}
            />
          </noscript> */}
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
