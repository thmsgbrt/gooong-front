import React, { Component } from 'react';
import ForgotPassword from '../modules/forgotPassword/containers/ForgotPassword';

class ForgotPasswordPage extends Component {
  render() {
    return <ForgotPassword />;
  }
}

export default ForgotPasswordPage;
