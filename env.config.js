/**
 * Regularly import this file on either server or client and have fun with it.
 * DO NOT PUT SENSITIVE INFORMATION LIKE ACCESS DATA HERE, THIS FILE MIGHT BE BUNDLED TO THE CLIENT!
 * Instead put it in a config file in the server folder and only require it from there.
 */

// define all environments you expect here
const environments = {
  development: {
    PUBLIC_URL: 'http://localhost:3000',
    API_URL: 'http://localhost:3001/graphql',
  },
  staging: {
    PUBLIC_URL: 'http://staging.example.com',
  },
  production: {
    PUBLIC_URL: 'https://www.successbook.app',
    API_URL: 'https://tg-successbook.herokuapp.com/graphql',
  },
};

// could be imported by the client or the server
const env = process.env.NODE_ENV;
module.exports = environments[env.toLowerCase()];
